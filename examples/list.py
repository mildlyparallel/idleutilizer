#!/usr/bin/python3

import xmlrpc.client
from pprint import pprint as pp

with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
    pp(proxy.requests_list())
