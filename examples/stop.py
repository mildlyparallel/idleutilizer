#!/usr/bin/python3

import xmlrpc.client
import sys
from pprint import pprint as pp

id_ = int(sys.argv[1])

pp('Stopping ' + str(id_))

with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
    pp(proxy.requests_stop(id_))
