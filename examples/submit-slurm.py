#!/usr/bin/python3

import xmlrpc.client

with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
    tmpl = '''#!/bin/bash
##SBATCH -p cpu
#SBATCH -t $time
#SBATCH -N $nodes
#SBATCH -n $cpu
#SBATCH --output $output
#SBATCH --error  $error

mpirun -n $nodes /home/kuchumov/idleutilizer/a.out
# mpirun -n $nodes /nfs/hybrilit.jinr.ru/user/k/kuchumov/idleutilizer/a.out

#$key1
#$user_data_str
'''

req = {
    'resources': {
        'nodes': 1,
        'cpu': 1,
        'memory': 4096,
        'time': 120
    },
    'scheduler': {
        'name': 'slurm'
    },
    'user_data': {
        'key1': 'val1',
        'user_data_str': 'str1',
        'user_data_int': 42,
        'user_data_float': 4.2,
        'user_data_bool': True
    },
    'template': tmpl,
    'hooks': [
        {
            'stage': 'executing',
            'condition': 'after',
            'command': '/root/hook1.sh %(remote_request_path)s',
            'host': 'kuchumov@ant.apmath.spbu.ru',
            'remote_root': '/home/kuchumov/idleutilizer'
        }
    ],
}

print(proxy.requests_submit(req))
