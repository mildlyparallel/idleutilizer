#!/usr/bin/python3

import xmlrpc.client

with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
    tmpl = '''universe = parallel
machine_count = $nodes
request_cpus = 1
requirements = $requirements
executable = /mnt/mpi/tests/openmpiscript-kut
arguments = hello-openmpi-3.0.0-1.bin
Iwd    = $root
log    = $log
output = $output
error  = $error
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
transfer_input_files = /mnt/mpi/tests/hello-openmpi-3.0.0-1.bin
queue
# fff
    '''

req = {
    'resources': {
        'nodes': 2,
        'cpu': 1,
        'memory': 4096,
    },
    'backend': {
        'name': 'opennebula',
        'owner': { 'uid': -1, 'gid': -1 },
    },
    'scheduler': {
        'name': 'htcondor'
    },
    'hooks': [
        {
            'name': 'test-hook',
            'command': '/root/hook1.sh %(remote_request_path)s',
            'stage': 'executing',
            'condition': 'after',
        }
    ],
    'user_data': {
        'key1': 'val1',
        'user_data_str': 'str1',
        'user_data_int': 42,
        'user_data_float': 4.2,
        'user_data_bool': True
    },
    'template': tmpl,
}

print(proxy.requests_submit(req))
