## Installation and usage

### Requirements

Make sure you have `python3` and `pip` installed:

```sh
yum -y install https://centos7.iuscommunity.org/ius-release.rpm
yum -y install python36u python36u-pip
ln -s $(which python3.6) /usr/bin/python3
```

Also make sure that you can connect via SSH to HTCondor Central Node:

```sh
ssh mpi@10.93.221.164
# Last login: Sun Feb  4 19:56:58 2018 from 10-93-221-100.jinr.ru
```

and that you have cloud's SSL cerificates installed (if needed):

```sh
file /etc/pki/ca-trust/source/anchors/onecloud-xmlrpc.pem
# /etc/pki/ca-trust/source/anchors/onecloud-xmlrpc.pem: PEM certificate
update-ca-trust extract && update-ca-trust enable
```

You'll also need 8000 port reachable from external network:

```sh
iptables -A IN_public_allow -p tcp -m tcp --dport 8000 -m conntrack --ctstate NEW -j ACCEPT
```

### Installation

```sh
git clone https://git.jinr.ru/cloud-team/SmartScheduler-IdleUtilizer
cd SmartScheduler-IdleUtilizer
git checkout dev/v2
python3 setup.py install
```

### Configuration

Generate configuration file

```sh
idle-utilizer -g /etc/idleutilizer/config.cfg
```

Specify the following paramters in configuration file (`/etc/idleutilizer/config.cfg`):

```ini
[cloud.opennebula]
# OpenNebula session string
one_auth = user:passord 
# OpenNebula XML-RPC endpoint
endpoint = https://cloud.jinr.ru/RPC2 
# Template id 
template_id = 1174
# ...
[scheduler.htcondor]
# HTCondor submit host (ssh)
host = user@htcondor.submit.node
# ...
[scheduler.slurm]
host = user@slurm.submit.node
# Path for storing IU job directories
remote_root = /tmp/
# ...
[daemon]
daemonize = 1
```

Create HTCondor default job template file (`/etc/idleutilizer/job.tmpl`). For example:

```conf
universe = parallel
machine_count = $host_count
request_cpus = 1
requirements = $requirements
executable = /mnt/mpi/tests/openmpiscript-kut
arguments = hello-openmpi-3.0.0-1.bin
log    = $log
output = $output
error  = $error
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
transfer_input_files = /mnt/mpi/tests/hello-openmpi-3.0.0-1.bin
queue
```
Now you can start Idle-Utilizer and submit a new request (see Examples section)

```sh
idle-utilizer
```

Make sure it has started successfully:

```sh
tail /var/log/idleutilizer.log
# 2018-02-04 20:22:35,489 - INFO - Starting Daemon, pidfile: /tmp/idleutilizer/pidfile.pid, chdir: /usr/local/bin/
# 2018-02-04 20:22:35,495 - WARNING - Starting daemon.
# 2018-02-04 20:22:35,534 - INFO - Serving XML-RPC server at 0.0.0.0:8000/RPC2
```

## Some important default paths:

*  `/etc/idleutilizer/config.cfg` – Configuration file
*  `/var/log/idleutilizer.log` – Log file
*  `/var/lib/idleutilizer/requests.json` – Database file
*  `/tmp/idleutilizer/*.job` – Local jobs files 
*  `user@htcondor.submit.node:/tmp/*.job` – Remote jobs files 
*  `/tmp/idleutilizer/pidfile.pid` – Daemon pidfile

## Job template varaibles

*  `$id` – Request ID
*  `$root` – Path to the working directory of the task
*  `$output` – Standart output file (absolute path)
*  `$error` – Standart errors file (absolute path)
*  `$log` – Job log file  (absolute path)
*  `$nodes` – The number of hosts
*  `$cpu` – The number of threads per node
*  `$memory` – The amount of memory per node
*  `$time` – The amount of preserved for computation (used only by Slurm)
*  `$requirements` – Job hosts requirement, a string in format `((Machine == "host1") || (Machine == "host2"))`
*  **All variables specified in request's `user_data` section are also passed to the template**

## XML-RPC Server

Idle-Utilizer forks XML-RPC server. It provides an API for
external services. Right now it supports the following methods:

* `requests_list` &ndash; Prints all requests in the database.
* `requests_find(int id)` &ndash; Returns request with specified id
* `requests_submit(dict req)` &ndash; Stores request object in the database for futher processing
* `requests_stop(int id)` &ndash; Stops request with specified ID, (changes its status to terminating)
* `requests_delete(int id)` &ndash; Marks request with 'delete_when_done' flag

### Request object Example

```python
{
	'id': 10,
	'status': 'executing',
	'created_at': 1512462490,
	'status_updated_at': 1512462493,
	'resources': {
		'nodes': 2,
		'cpu': 2,
		'memory': 2048
	},
	'backend': {
		'name': 'opennebula',
		'owner': {'gid': -1, 'uid': -1},
		'hosts': [
			{'chown': True, 'id': 72762, 'ip': '10.93.221.67'},
			{'chown': True, 'id': 72763, 'ip': '10.93.221.74'}
		],
	},
	'scheduler': {
		'name': 'htcondor',
		'hostnames': ['10-93-221-74.jinr.ru', '10-93-221-67.jinr.ru']
	},
	'job': {
		'id': 64763,
		'localdir': '/tmp/34',
		'remotedir': '/tmp/34',
		'status': 'done'
	},
	'user_data': {
		'key1': 'value1'
		# Job parametres
	},
	'error': 'Error message'
}
```

### Request stages

1. `pending` Specified amount (`host_count`) of virtual machines is requested. IDs are stored in `hosts` list
2. `booting` IPs of virtual machines are requested and stored in `hosts` list
2. `transferring` VMs are being transferred to the owner, specified in request
3. `bootstrapping` Hostnames from job scheduler are requested and stored in `hosts` list
3. `preparing` Job file is created locally
3. `copying` Remote directory is created and job file is moved there (on scheduler's hosts)
4. `submitting` Job file is subbmitted for execution
5. `executing` Job is being executed.
6. `terminating` Virtual machines are requested to terminate
7. `done` Virtual machines have terminated, request is deleted if necesseray 

### Hooks

Custom scripts can be exectuted before and after each stage of request proccessing and when an error occurs.
These scripts are specified in the `hooks` field of the request:

```python
'hooks': [
	{
		'command': '/root/hook1.sh %(remote_request_path)s',
		'stage': 'executing',
		'condition': 'after',
		'host': 'user@submit.node',
		'remote_root': '/tmp'
	}
],
```

This hook will be executed `after` the `executing` stage is finished. 
`hooks1.sh` should be located on the machine running IdleUtilizer, as the following command
will be executed:

```bash
ssh user@submit.node /bin/bash -s -- < /root/hook1.sh /tmp/37/request.conf
```

File `/tmp/37/request.conf` will be created just before the hook execution at remote `host`.
It contains request fields in `key=value\n` format.
This file can be sourced inside hook script to access request data:

```bash
resources__nodes=1
resources__cpu=1
resources__memory=4096
resources__time=120
scheduler__name="slurm"
job__id=28372
job__status="done"
job__localdir="/tmp/43"
job__remotedir="/home/kuchumov/idleutilizer/43"
# ...
```

The following command variables are supprted:

1. `%(request_id)s` – Request ID
3. `%(hook_id)s` – Hook ID
4. `%(name)s` – Hook name
6. `%(local_root)s` – Path to the root directory on the local machine 
7. `%(local_dir)s` – Path to the request directory on the local machine 
7. `%(local_request_path)s` – Path to the request file on the local machine
8. `%(remote_root)s` – Remote host root directory
9. `%(remote_dir)s` – Path to the request directory on the remote machine 
10. `%(remote_request_path)` – Path to the request file on the remote machine
11. `%(host)s` – Name of the remote machine (for ssh'ing)

The following hook request fields are supported. These variables overwrite default values.

1. `id` – Hook id
2. `name` – Hook name
4. `stage` – Hook exection stage (`pending`, `booting` etc)
5. `condition` – When this hook will be executed (`after`, `before`, `error`) 
6. `command` – Command for exection with afromentioned template variables
7. `command_template` – Command template (ssh stuff) with `%(command)s` variable
8. `remote_root` – Remote host root directory
9. `remote_dir` – Path to the request directory on the remote machine 
11. `remote_request_path` – Path to the request file on the remote machine
12. `host` – Name of the remote machine (for ssh'ing)

### Examples

Check `examples` directory in this repository.
