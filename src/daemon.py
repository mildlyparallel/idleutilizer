from daemonize import Daemonize
import os

from pprint import pprint as pp

def export_config():
    return (Daemon.section, Daemon.params)

class Daemon():
    section = 'daemon'

    params = [
        {
            'name': 'daemonize',
            'default': '0',
            'type': int
        },
        {
            'name': 'appname',
            'default': 'smartscheduler',
            'type': str,
        },
        {
            'name': 'pidfile',
            'default': '/tmp/idleutilizer/pidfile.pid',
            'type': str
        },
        {
            'name': 'verbose',
            'default': '0',
            'type': int
        },
        {
            'name': 'chdir',
            'default': '/usr/local/bin/',
            'type': str
        },
    ]

    def __init__(self, config, logger):
        values = config.get_values(self.section, self.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._logger = logger

    def fork(self, callback):
        self._logger.info('Starting Daemon, pidfile: {}, chdir: {}'
            .format(self.pidfile, self.chdir))

        keep_fds = [self._logger.fileno]

        dir = os.path.dirname(self.pidfile)
        if not os.path.exists(dir):
            os.makedirs(dir)

        daemon = Daemonize(
            action=callback,
            app=self.appname,
            pid=self.pidfile,
            verbose=self.verbose,
            chdir=self.chdir,
            logger=self._logger,
            keep_fds=keep_fds
        )
        # TODO: redirect stderr to logfiles
        daemon.start()
