from pprint import pprint as pp

class RPCApi:
    def __init__(self, config, logger, requests):
        self._logger = logger
        self._reqests = requests

    def requests_list(self):
        try:
            all = self._reqests.list()
        except Exception as e:
            return {'error': str(e)}

        output = []

        for request in all:
            id, r = request
            r['id'] = id
            output.append(dict(r))

        return output

    def requests_find(self, id):
        if type(id) != int or id < 0:
            return {'error': 'Invalid request id'}

        try:
            found = self._reqests.find(id)
        except Exception as e:
            return {'error': str(e)}

        if found == None:
            return {'error': 'Request is not found'}

        found['id'] = id
        return dict(found)

    def requests_submit(self, request):
        if type(request) != dict:
            return {'error': 'Invalid argument type, dict expected'}

        try:
            id = self._reqests.submit(request)
        except Exception as e:
            return {'error': str(e)}

        if id == None:
            return {'error': 'Cant submit request'}

        return id

    def requests_stop(self, id):
        if type(id) != int or id < 0:
            return {'error': 'Invalid request id'}

        try:
            self._reqests.stop(id)
        except Exception as e:
            return {'error': str(e)}

        return id

    def requests_delete(self, id):
        if type(id) != int or id < 0:
            return {'error': 'Invalid request id'}

        try:
            self._reqests.delete(id)
        except Exception as e:
            return {'error': str(e)}

        return id
