from pprint import pprint as pp
from threading import Lock

from time import time
from time import sleep

def now():
    return int(time())

def export_config():
    return (Request.section, Request.params)

class Mapper(object):
    def __init__(self):
        self.data = {}

    def get_dict(self):
        out = {}

        for k, v in self.data.items():
            if v == None:
                continue
            if isinstance(v, Mapper):
                out[k] = v.get_dict()
            elif type(v) == list:
                out[k] = []
                for i in v:
                    if isinstance(i, Mapper):
                        out[k].append(i.get_dict())
                    else:
                        out[k].append(i)
            else:
                out[k] = v

        return out


class Resources(Mapper):
    def __init__(self, res, parent):
        super(Resources, self)

        self._parent = parent

        self.data = {
            'nodes':   int(res.get('nodes',   self._parent.config['default_nodes'])),
            'cpu':     int(res.get('cpu',     self._parent.config['default_cpu'])),
            'memory':  int(res.get('memory',  self._parent.config['default_memory'])),
            'time':    int(res.get('time',    self._parent.config['default_time']))
        }

        self._parent = parent

    @property
    def config(self):
        return self._parent.config

    @property
    def nodes(self):
        return self.data['nodes']

    @property
    def cpu(self):
        return self.data['cpu']

    @property
    def memory(self):
        return self.data['memory']

    @property
    def time(self):
        return self.data['time']

class Hook(Mapper):
    def __init__(self, hook_id, hook, parent):
        super(Hook, self)

        if 'command' not in hook:
            raise Exception('Hook command is not specified')

        self.data = {
            'name':                 hook.get('name', None),
            'stage':                hook.get('stage', None),
            'command':              hook.get('command', None),
            'command_template':     hook.get('command_template', None),
            'condition':            hook.get('condition', None),
            'request_transfer':     hook.get('request_transfer', None),
            'format':               hook.get('format', None),
            'remote_root':          hook.get('remote_root', None),
            'remote_dir':           hook.get('remote_dir', None),
            'remote_request_path':  hook.get('remote_request_path', None),
            'host':                 hook.get('host', None),
            'repeat':               hook.get('repeat', None),
            'return_code':          hook.get('return_code', None),
            'stdout':               hook.get('stdout', None),
            'stderr':               hook.get('stderr', None),
        }

        self.data['id'] = hook_id

        self._parent = parent

    @property
    def id_(self):
        return self.data['id']

    @property
    def name(self):
        return self.data['name']

    @property
    def stage(self):
        return self.data['stage']

    @property
    def command(self):
        return self.data['command']

    @property
    def command_template(self):
        return self.data['command_template']

    @property
    def condition(self):
        return self.data['condition']

    @property
    def repeat(self):
        return self.data['repeat']

    @property
    def request_transfer(self):
        return self.data['request_transfer']

    @property
    def format(self):
        return self.data['format']

    @property
    def remote_root(self):
        return self.data['remote_root']

    @property
    def remote_dir(self):
        return self.data['remote_dir']

    @property
    def remote_request_path(self):
        return self.data['remote_request_path']

    @property
    def host(self):
        return self.data['host']

    @property
    def return_code(self):
        return self.data['return_code']

    @return_code.setter
    def return_code(self, rc):
        self.data['return_code'] = rc
        self._parent.save()

    @property
    def stderr(self):
        return self.data['stderr']

    @stderr.setter
    def stderr(self, rc):
        self.data['stderr'] = rc
        self._parent.save()

    @property
    def stdout(self):
        return self.data['stdout']

    @stdout.setter
    def stdout(self, rc):
        self.data['stdout'] = rc
        self._parent.save()


class BackendHost(Mapper):
    def __init__(self, host, parent):
        super(BackendHost, self)

        if 'id' not in host:
            raise Exception('Host id is not scpecified')

        self.data = {
            'id': host['id'],
            'ip': host.get('ip', None),
            'chown': host.get('chown', None)
        }

        self._parent = parent

    @property
    def config(self):
        return self._parent.config

    @property
    def id_(self):
        return self.data['id']

    @property
    def ip(self):
        return self.data['ip']

    @ip.setter
    def ip(self, ip):
        self.data['ip'] = ip
        self._parent.save()

    @property
    def chown(self):
        return self.data['chown']

    @chown.setter
    def chown(self, t):
        self.data['chown'] = True
        self._parent.save()

class Backend(Mapper):
    def __init__(self, backend, parent):
        super(Backend, self)

        self._parent = parent

        self.data = {}
        self.data['name'] = backend.get('name', None)
        self.data['owner'] = backend.get('owner', None)
        self.data['hosts'] = []
        for h in backend.get('hosts', []):
            self.data['hosts'].append(BackendHost(h, self._parent))

    @property
    def config(self):
        return self._parent.config

    @property
    def name(self):
        return self.data['name']

    @property
    def owner(self):
        return self.data['owner']

    @property
    def hosts(self):
        return self.data.get('hosts', [])

    def append_host(self, id_, ip = None):
        h = {'id': id_, 'ip': ip}

        self.data['hosts'].append(BackendHost(h, self._parent))
        self._parent.save()


class Scheduler(Mapper):
    def __init__(self, scheduler, parent):
        super(Scheduler, self)

        self.data = {
            'name': scheduler.get('name', None),
            'hostnames': scheduler.get('hostnames', [])
        }
        self._parent = parent

    @property
    def config(self):
        return self._parent.config

    @property
    def name(self):
        return self.data['name']

    @property
    def hostnames(self):
        return self.data['hostnames']

    def append_hostname(self, hostname):
        self.data['hostnames'].append(hostname)
        self._parent.save()

    def remove_hostname(self, hostname):
        if hostname not in self.data['hostnames']:
            return

        self.data['hostnames'].remove(hostname)
        self._parent.save()

class Job(Mapper):
    def __init__(self, job, parent):
        super(Job, self)

        self.data = {
            'id':         job.get('id', None),
            'status':     job.get('status', None),
            'localdir':   job.get('localdir', None),
            'remotedir':  job.get('remotedir', None),
        }
        self._parent = parent

    @property
    def config(self):
        return self._parent.config

    @property
    def id_(self):
        return self.data['id']

    @id_.setter
    def id_(self, i):
        self.data['id'] = i
        self._parent.save()

    @property
    def status(self):
        return self.data['status']

    @status.setter
    def status(self, st):
        self.data['status'] = st
        self._parent.save()

    @property
    def localdir(self):
        return self.data['localdir']

    @localdir.setter
    def localdir(self, d):
        self.data['localdir'] = d
        self._parent.save()

    @property
    def remotedir(self):
        return self.data['remotedir']

    @remotedir.setter
    def remotedir(self, d):
        self.data['remotedir'] = d
        self._parent.save()

class Request(Mapper):
    section = 'requests'

    params = [
        { 
            'name': 'default_nodes',
            'default': '1',
            'type': int
        },
        {
            'name': 'default_cpu',
            'default': '4',
            'type': int
        }, 
        {
            'name': 'default_memory',
            'default': '4096',
            'type': int
        },
        {
            'name': 'default_time',
            'default': '100',
            'type': int
        },
    ]

    config = None

    logger = None

    storage = None

    @staticmethod
    def set_config(reader):
        Request.config = {}
        values = reader.get_values(Request.section, Request.params);
        for param in values:
            Request.config[param] = values[param]

    @staticmethod
    def set_logger(logger):
        Request.logger = logger

    @staticmethod
    def set_storage(storage):
        Request.storage = storage

    def __init__(self, req, save = True, id_ = None):
        super(Request, self)

        try:
            self.data = {
                'resources':          Resources(req.get('resources', {}), self),
                'backend':            Backend(req.get('backend', {}), self),
                'scheduler':          Scheduler(req.get('scheduler', {}), self),
                'hooks':              [],
                'job':                Job(req.get('job', {}), self),
                'template':           req.get('template', None),
                'user_data':          req.get('user_data', {}),
                'status':             req.get('status', 'pending'),
                'created_at':         req.get('created_at', now()),
                'status_updated_at':  req.get('status_updated_at', now()),
                'error':              req.get('error', None),
                'delete_when_done':   req.get('delete_when_done', None),
            }

            for hook_id, h in enumerate(req.get('hooks', [])):
                self.data['hooks'].append(Hook(hook_id, h, self))

            self._id = req.get('id', id_)
            
        except Exception as e:
            raise Exception('Can not parse request parameters')

        if save:
            self._id = Request.storage.create(self.get_dict())

    def delete(self):
        Request.storage.delete(self._id)

    def save(self):
        Request.storage.update(self._id, self.get_dict())

    @staticmethod
    def list(return_dict = False):
        d = Request.storage.list()
        if return_dict:
            return d

        ret = []
        for i,r in d:
            r['id'] = i
            ret.append(Request(r, False, i))

        return ret 

    @staticmethod
    def find(id_, return_dict = False):
        d = Request.storage.find(id_)
        if return_dict:
            return d
        if d:
            return Request(d, False, id_)
        return None

    @property
    def id_(self):
        return self._id

    @property
    def resources(self):
        return self.data['resources']

    @property
    def backend(self):
        return self.data['backend']

    @property
    def scheduler(self):
        return self.data['scheduler']

    @property
    def hooks(self):
        return self.data['hooks']

    @property
    def job(self):
        return self.data['job']

    @property
    def user_data(self):
        return self.data['user_data']

    @property
    def created_at(self):
        return self.data['created_at']

    @property
    def template(self):
        return self.data['template']

    @template.setter
    def template(self, template):
        self.data['template'] = template
        self.save()

    @property
    def status(self):
        return self.data['status']

    @status.setter
    def status(self, value):
        self.data['status_updated_at'] = now()
        self.data['status'] = value
        self.save()

    @property
    def created_at(self):
        return self.data['created_at']

    @property
    def status_updated_at(self):
        return self.data['status_updated_at']

    @property
    def error(self):
        return self.data['error']

    @error.setter
    def error(self, error):
        self.data['error'] = error
        self.save()

    @property
    def delete_when_done(self):
        return self.data['delete_when_done']

    @delete_when_done.setter
    def delete_when_done(self, delete_when_done):
        self.data['delete_when_done'] = delete_when_done
        self.save()

