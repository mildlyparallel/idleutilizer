from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

class MyXMLRPCServer(SimpleXMLRPCServer):
    def __init__(self, logger, host, port, path = '/', whitelist = [], log_requests=True):
        self.logger = logger
        self.whitelist = whitelist

        self.rh = SimpleXMLRPCRequestHandler
        self.rh.rpc_path = path

        super(MyXMLRPCServer, self).__init__(
            addr=(host, port),
            requestHandler=self.rh,
            logRequests=log_requests
        )

        self.register_introspection_functions()

    def verify_request(self, request, client_address):
        if len(self.whitelist) == 0:
            return True

        (host, port) = client_address

        if host in self.whitelist:
            return True

        self.logger.debug(
            'Blocking connection from {}:{} (not unauthorized)'
            .format(host, port)
        )

        return False

from threading import Thread 

from pprint import pprint as pp

def export_config():
    return (RPCServer.section, RPCServer.params)

class RPCServer():
    section = 'rpcserver'

    params = [
        {
            'name': 'path',
            'default': '/RPC2',
            'type': str
        },
        {
            'name': 'host',
            'default': '0.0.0.0',
            'type': str
        }, 
        {
            'name': 'port',
            'default': '8000',
            'type': int
        }, 
        {
            'name': 'log_requests',
            'default': 'False',
            'type': bool
        }, 
        {
            'name': 'whitelist',
            'default': '[]',
            'type': eval
        } 
    ]

    def __init__(self, config, logger):
        values = config.get_values(self.section, self.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._logger = logger

        self._thread = None

        self._configure_server()

    def _configure_server(self):
        self._handle = MyXMLRPCServer(
            self._logger,
            self.host, self.port, self.path,
            self.whitelist, self.log_requests
        )

    def register_api(self, api):
        self._handle.register_instance(api)

    def serve(self):
        self._logger.info("Serving XML-RPC server at {}:{}{}"
            .format(self.host, self.port, self.path))

        self._handle.serve_forever()

    def fork(self):
        self._thread = Thread(target=self.serve)
        self._thread.daemon = True
        self._thread.start()

    def join(self):
        self._logger.info("Stopping XML-RPC server")

        self._handle.server_close()

        if self._thread == None:
            return

        self._handle.shutdown()
        self._thread.join()

