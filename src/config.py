import configparser
import importlib
from distutils import util;

class Config(object):
    def __init__(self, path):
        self._parser = configparser.RawConfigParser()
        self._parser.read(path)

    def get_values(self, section, parameters):
        config = dict(self._parser.items(section))

        values = {}

        for param in parameters:
            name = param['name']

            if name in config:
                if param['type'] == bool:
                    values[name] = util.strtobool(config[name])
                else:
                    values[name] = param['type'](config[name])
                continue

            if param['default'] != None:
                values[name] = param['type'](param['default'])
                continue

            raise Exception("Configuration parameter '{}' in '{}' section is missing"
                .format(name, section))

        return values

    def get_items(self, section):
        return self._parser.items(section)

