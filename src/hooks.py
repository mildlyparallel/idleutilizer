
import os
import subprocess
from pprint import pprint as pp

def export_config():
    return (Hooks.section, Hooks.params)

class Hooks(object):
    section = 'hooks'

    params = [
        { 
            'name': 'host',
            'default': 'user@htcondor.submit.node',
            'type': str
        },
        {
            'name': 'command_template',
            'default': 'ssh %(host)s /bin/bash -s -- < %(command)s',
            'type': str
        },
        {
            'name': 'local_root',
            'default': '/tmp/idleutilizer/',
            'type': str
        },
        {
            'name': 'remote_root',
            'default': '/tmp/',
            'type': str
        },
        {
            'name': 'request_format',
            'default': 'conf',
            'type': str
        },
        {
            'name': 'request_transfer',
            'default': 'scp -r %(local_dir)s %(host)s:%(remote_root)s',
            'type': str
        }
    ]

    def __init__(self, config, logger):
        values = config.get_values(Hooks.section, Hooks.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._logger = logger

    def __del__(self):
        pass

    def _create_hook_context(self, request, hook):
        ctx = {};

        hook_id = str(hook.id_)
        req_id = str(request.id_)
        ctx['request_id'] = request.id_

        ctx['hook_id'] = hook_id

        ctx['name'] = 'hook_' + hook_id
        if hook.name:
            ctx['name'] = hook.name

        ctx['command'] = hook.command

        ctx['format'] = hook.format
        if not hook.format:
            ctx['format'] = self.request_format
        ctx['request_filename'] = 'request.' + ctx['format']

        ctx['local_root'] = self.local_root
        ctx['local_dir'] = os.path.join(ctx['local_root'], req_id)
        ctx['local_request_path'] = os.path.join(ctx['local_dir'], ctx['request_filename'])

        if not os.path.exists(ctx['local_dir']):
            os.makedirs(ctx['local_dir'])

        ctx['remote_root'] = self.remote_root
        if hook.remote_root:
            ctx['remote_root'] = hook.remote_root

        ctx['remote_dir'] = os.path.join(ctx['remote_root'], req_id)
        if hook.remote_dir:
            ctx['remote_dir'] = hook.remote_dir

        ctx['remote_request_path'] = os.path.join(ctx['remote_dir'], ctx['request_filename'])
        if hook.remote_request_path:
            ctx['remote_request_path'] = hook.remote_request_path

        ctx['command_template'] = self.command_template
        if hook.command_template:
            ctx['command_template'] = hook.command_template

        ctx['request_transfer'] = self.request_transfer
        if hook.request_transfer:
            ctx['request_transfer'] = hook.request_transfer

        ctx['host'] = self.host
        if hook.host:
            ctx['host'] = hook.host

        return ctx

    def _create_request_file(self, ctx, request):
        if ctx['format'] == 'conf':
            content = self._convert_to_conf(request.get_dict())
        else:
            raise Exception('Request format %s is not supported' % fmt)

        with open(ctx['local_request_path'], 'w') as f:
            f.write(content)

    def _transfer_request_file(self, ctx):
        cmd = ctx['request_transfer'] % ctx
        self._exec(cmd)

    def _execute_hook(self, ctx):
        c = ctx.copy()
        c['command'] = ctx['command'] % ctx

        tmpl_cmd = ctx['command_template'] % c

        return self._exec(tmpl_cmd)

    def _convert_to_conf(self, data, prefix = ''):
        if type(data) == str:
            return prefix + '="' + data.replace('\n', '\\n') + '"\n'
        if type(data) == str or type(data) == int or type(data) == float:
            return prefix + '=' + str(data) + '\n'

        if prefix != '':
            prefix += '__'

        output = ''

        if type(data) == list:
            for k, v in enumerate(data):
                output += self._convert_to_conf(v, prefix + str(k))

        if type(data) == dict:
            for k, v in data.items():
                output += self._convert_to_conf(v, prefix + k)

        return output

    def exec(self, request, stage, condition):
        for hook in request.hooks:
            if hook.stage != stage and hook.stage != 'any':
                continue

            if not hook.condition:
                hook.condition = 'after'

            if hook.condition != condition:
                continue

            if hook.return_code and not hook.repeat:
                continue

            ctx = self._create_hook_context(request, hook)

            self._logger.info('req #%s - Executing hook %s' 
                % (request.id_, ctx['name']))

            self._create_request_file(ctx, request)

            self._transfer_request_file(ctx)

            (hook.return_code, hook.stdout, hook.stderr) = self._execute_hook(ctx)

            if hook.return_code != 0:
                self._logger.warning('req #%s - Hook %s failed'
                    % (request.id_, ctx['name']))

    def _exec(self, cmd):
        # TODO: the same function is in libs.schedulers.htcondor

        self._logger.debug('Executing: ' + cmd)
        sp = subprocess.Popen(cmd, shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        (out, err) = sp.communicate(input=None)

        self._logger.debug('cmd pid:%s' % sp.pid)
        self._logger.debug('cmd ret:%s' % sp.returncode)
        self._logger.debug('cmd out:%s' % out)
        self._logger.debug('cmd err:%s' % err)

        return (sp.returncode, out.decode('utf-8'), err.decode('utf-8'))

