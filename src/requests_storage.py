import os
from pprint import pprint as pp
from tinydb import TinyDB, where
from threading import Lock

def export_config():
    return (RequestsStorage.section, RequestsStorage.params)

class RequestsStorage():
    section = 'requests_storage'

    params = [
        {
            'name': 'db_path',
            'default': '/var/lib/idleutilizer/requests.json',
            'type': str
        } 
    ]

    def __init__(self, config, logger):
        values = config.get_values(self.section, self.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._logger = logger

        self._lock = Lock()

        dir = os.path.dirname(self.db_path)
        if dir and not os.path.exists(dir):
            os.makedirs(dir)

        self._db = TinyDB(self.db_path)

    def __del__(self):
        self._db.close()

    def create(self, fields):
        self._lock.acquire()

        try:
            id = self._db.insert(fields)
        finally:
            self._lock.release()

        return id

    def _get(self, id):
        self._lock.acquire()
        try:
            found = self._db.get(doc_id = id)
        finally:
            self._lock.release()

        if not found:
            return None

        return found

    def _query(self, query):
        key, val = query.popitem()

        self._lock.acquire()
        try:
            found = self._db.search(where(key) == val)
        finally:
            self._lock.release()

        ret = []

        for el in found:
            ret.append((el.doc_id, el))

        return ret

    def list(self):
        ret = []
        for el in self._db.all():
            ret.append((el.doc_id, el))
        return ret

    def find(self, query):
        if isinstance(query, int):
            return self._get(query)

        return self._query(query)

    def update(self, query, fields):
        self._lock.acquire()
        try:
            if isinstance(query, int):
                if self._db.contains(doc_ids=[query]):
                    self._db.update(fields, doc_ids = [query])
            else:
                key, val = query.popitem()
                self._db.update(fields, where(key) == val)
        finally:
            self._lock.release()


    def delete(self, query):
        self._lock.acquire()
        try:
            if isinstance(query, int):
                if self._db.contains(doc_ids=[query]):
                    self._db.remove(doc_ids = [query])
            else:
                key, val = query.popitem()
                self._db.remove(where(key) == val)
        finally:
            self._lock.release()
