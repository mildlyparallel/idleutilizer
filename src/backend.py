import importlib

from pprint import pprint as pp

def export_config():
    return (Backend.section, Backend.modules + Backend.params)

class Backend():
    section = 'backend'

    modules = [
        {
            'name': 'opennebula',
            'default': 'libs.clouds.opennebula',
            'type': str
        }
    ]

    params = []

    def __init__(self, config, logger):
        values = config.get_values(Backend.section, Backend.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._drivers = {}
        for name, driver in config.get_items(Backend.section):
            found = False
            for d in Backend.params:
                if d['name'] == name:
                    found = True
                    break

            if found:
                continue

            module = importlib.import_module(driver)
            handle = module.init_handler(config, logger)
            self._drivers[name] = handle

        self._logger = logger

    # TODO: move these functions to Logger class
    def _info(self, id_, message):
        self._logger.info('req #%s - %s' % (id_, message))

    def _warning(self, id_, message):
        self._logger.warning('req #%s - %s' % (id_, message))

    def _error(self, id_, message):
        self._logger.error('req #%s - %s' % (id_, message))

    def spawn_hosts(self, request):
        if request.backend == None or request.backend.name == None:
            return

        handle = self._drivers[request.backend.name]

        nhosts = len(request.backend.hosts)

        left = request.resources.nodes - nhosts
        self._info(request.id_, 'Spawning %d hosts' % left)

        for i in range(nhosts, request.resources.nodes):
            params = {
                'name': 'iu-%s-%s' % (request.id_, i),
                'cpu': request.resources.cpu,
                'memory': request.resources.memory
            }

            vm_id = handle.spawn_host(params)
            request.backend.append_host(vm_id)

    def transfer_hosts(self, request):
        if request.backend.owner == None:
            return

        self._info(request.id_, 'transferring hosts to user account')

        handle = self._drivers[request.backend.name]

        for h in request.backend.hosts:
            if h.chown:
                continue

            handle.chown_host(h.id_, request.backend.owner)
            h.chown = True


    def sync_addresses(self, request):
        self._info(request.id_, 'waiting for hosts IP adresses')

        handle = self._drivers[request.backend.name]

        updated = handle.get_hosts()

        for h in request.backend.hosts:
            if h.ip:
                continue

            for hh in updated:
                if hh['id'] != h.id_:
                    continue
                if 'ip' in hh and hh['ip']:
                    h.ip = hh['ip']
                    break

    def terminate_hosts(self, request):
        if not request.backend or not request.backend.name:
            return

        handle = self._drivers[request.backend.name]

        for h in request.backend.hosts:
            handle.remove_host(h.id_)


