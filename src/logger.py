import logging
from pprint import pprint as pp

def export_config():
    return ('logging', params)

params = [
    {
        'name': 'name',
        'default': 'smartscheduler',
        'type': str
    },
    {
        'name': 'level',
        'default': 'INFO',
        'type': str
    },
    {
        'name': 'filename',
        'default': '/var/log/idleutilizer.log',
        'type': str
    },
    {
        'name': 'format',
        'default': '%(asctime)s - %(levelname)s - %(message)s',
        'type': str
    },
    {
        'name': 'debug_format',
        'default': '%(asctime)s - %(module)s - %(levelname)s - %(message)s',
        'type': str
    },
]

def get_logger_handler(config):
    cfg = config.get_values('logging', params);

    logger = logging.getLogger(cfg['name'])
    level = logging.getLevelName(cfg['level'])
    logger.setLevel(level)

    handler = logging.FileHandler(cfg['filename'])
    if (cfg['level'] == 'DEBUG'):
        formatter = logging.Formatter(cfg['debug_format'])
    else:
        formatter = logging.Formatter(cfg['format'])

    handler.setFormatter(formatter)
    logger.addHandler(handler)

    logger.__dict__['fileno'] = handler.stream.fileno()

    return logger
