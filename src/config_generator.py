import configparser
import importlib
import os

from pprint import pprint as pp

class ConfigGenerator(object):
    modules = [
        'src.backend',
        'libs.clouds.opennebula',
        'src.scheduler',
        'libs.schedulers.htcondor',
        'libs.schedulers.slurm',
        'src.hooks',
        'src.requests',
        'src.requests_storage',
        'src.rpcapi',
        'src.daemon',
        'src.rpcserver',
        'src.logger'
    ]

    def __init__(self, path):
        self._path = path
        self._config = configparser.ConfigParser()

    def _import_config(self, path):
        module = importlib.import_module(path)

        if not hasattr(module, 'export_config'):
            return

        section, params = module.export_config()

        self._config[section] = {}

        for p in params:
            if not p['default']:
                continue

            k = p['name']
            v = str(p['default'])

            self._config[section][k] = v

    def generate(self):
        for path in self.modules:
            self._import_config(path)

        dir = os.path.dirname(self._path)
        if dir and not os.path.exists(dir):
            os.makedirs(dir)

        with open(self._path, 'w') as f:
            self._config.write(f)

        os.chmod(self._path, 0o600)

