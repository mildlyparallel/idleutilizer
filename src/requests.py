from pprint import pprint as pp
from tinydb import TinyDB, where
from threading import Lock

from time import time
from time import sleep

from src.request import Request

def now():
    return int(time())

def export_config():
    return (Requests.section, Requests.params)

class Requests():
    section = 'requests'

    params = [
        {
            'name': 'retry_on_error',
            'default': 'True',
            'type': bool
        }, 
        {
            'name': 'poll_interval',
            'default': '10',
            'type': int
        } 
    ]

    def __init__(self, config, logger, storage, backend, scheduler, hooks):
        self._backend = backend
        self._scheduler = scheduler
        self._hooks = hooks
        self._logger = logger

        values = config.get_values(Requests.section, Requests.params);
        for param in values:
            self.__dict__[param] = values[param]

        Request.set_storage(storage)
        Request.set_config(config)
        Request.set_logger(logger)

    def __del__(self):
        pass

    # TODO: move these functions to logger class
    def _info(self, id_, message):
        self._logger.info('req #%s - %s' % (id_, message))

    def _warning(self, id_, message):
        self._logger.warning('req #%s - %s' % (id_, message))

    def _error(self, id_, message):
        self._logger.error('req #%s - %s' % (id_, message))

    def submit(self, request_dict):
        req = Request(request_dict)
        
        self._info(req.id_, 'request submited')

        return req.id_

    def list(self):
        return Request.list(True)

    def find(self, id_):
        # TODO: assert arument types
        return Request.find(id_, True)

    def _handle_pending(self, req):
        if not req.backend or not req.backend.name:
            req.status = 'preparing'
            return

        self._backend.spawn_hosts(req)

        if len(req.backend.hosts) == req.resources.nodes:
            req.status = 'booting'

    def _handle_booting(self, req):
        self._backend.sync_addresses(req)

        for h in req.backend.hosts:
            if not h.ip:
                return

        req.status = 'transferring'

    def _handle_transferring(self, req):
        self._backend.transfer_hosts(req)

        for h in req.backend.hosts:
            if not h.chown:
                return

        req.status = 'bootstrapping'

    def _handle_bootstrapping(self, req):
        self._info(req.id_, 'waiting for hosts to appear in schedulers pool')

        self._scheduler.sync_backend_hosts(req)

        if len(req.scheduler.hostnames) == req.resources.nodes:
            req.status = 'preparing'

    def _handle_preparing(self, req):
        self._info(req.id_, 'preparing scheduler job files')

        self._scheduler.create_job(req)

        req.status = 'copying'

    def _handle_copying(self, req):
        self._info(req.id_, 'copying job files to remote')

        self._scheduler.transfer_job(req)

        req.status = 'submitting'

    def _handle_submitting(self, req):
        self._info(req.id_, 'transferring job files')

        self._scheduler.submit_job(req)

        req.status = 'executing'

    def _handle_executing(self, req):
        self._info(req.id_, 'waiting for job to finish')

        previous = req.status

        self._scheduler.describe_job(req)

        if previous != req.status:
            self._info(req.id_, 'job (id: %s) status has changed to %s' % (req.job.id_, req.job.status))

        if req.job.status == 'done':
            req.status = 'terminating' 

    def _handle_terminating(self, req):
        self._scheduler.stop_job(req)

        self._scheduler.remove_hosts(req)

        self._backend.terminate_hosts(req)

        req.status = 'done'

    def _handle_done(self, req):
        if not req.delete_when_done:
            return

        i = req.id_

        self._scheduler.cleanup_job(req)

        req.delete()

        self._info(i, 'Request is deleted')

    def _update_request(self, req):

        try:
            status = req.status

            self._hooks.exec(req, status, 'before')

            if status == 'done':
                self._handle_done(req)
            elif status == 'terminating':
                self._handle_terminating(req)
            elif status == 'executing':
                self._handle_executing(req)
            elif status == 'submitting':
                self._handle_submitting(req)
            elif status == 'copying':
                self._handle_copying(req)
            elif status == 'preparing':
                self._handle_preparing(req)
            elif status == 'bootstrapping':
                self._handle_bootstrapping(req)
            elif status == 'transferring':
                self._handle_transferring(req)
            elif status == 'booting':
                self._handle_booting(req)
            elif status == 'pending':
                self._handle_pending(req)
            else:
                self._error(req.id_, 'Unknown status: %s' % status)

            if status != req.status:
                self._info(req.id_, 'status has changed from %s to %s' 
                            % (status, req.status))
                hooks = self._hooks.exec(req, status, 'after')

        except Exception as e:
            self._logger.error(e, exc_info=True)
            req.error = str(e)

        try:
            if req.error:
                self._hooks.exec(req, status, 'error')
        except Exception as e:
            self._logger.error(e, exc_info=True)
            req.error = str(e)

    def update(self):
        for req in Request.list():
            if not self.retry_on_error and req.error:
                continue 

            self._update_request(req)

    def stop(self, id_):
        self._info(id_, 'stop request is recieved')

        req = Request.find(id_)

        if not req:
            return

        if req.status == 'terminating':
            return

        if req.status == 'done':
            return

        req.status = 'terminating'

        self._info(id_, 'status has changed from %s to terminating' % req.status)

    def delete(self, id_):
        self._info(id_, 'delete request is recieved')

        req = Request.find(id_)

        if not req:
            return

        req.delete_when_done = True

        self._info(id_, 'Request will be deleted when done')

    def loop(self):
        self._logger.debug('Starting Requests loop, interval: {}'
                .format(self.poll_interval))

        while True:
            self.update()
            sleep(self.poll_interval)

