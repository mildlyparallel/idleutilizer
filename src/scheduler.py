import os
import sys
from pprint import pprint as pp

import importlib

def export_config():
    return (Scheduler.section, Scheduler.modules + Scheduler.params)

class Scheduler():
    section = 'scheduler'

    modules = [
        {
            'name': 'htcondor',
            'default': 'libs.schedulers.htcondor',
            'type': str
        },
        {
            'name': 'slurm',
            'default': 'libs.schedulers.slurm',
            'type': str
        }
    ]

    params = [
    ]

    def __init__(self, config, logger):
        self._drivers = {}
        for name, driver in config.get_items(Scheduler.section):
            found = False
            for d in Scheduler.params:
                if d['name'] == name:
                    found = True
                    break

            if found:
                continue

            module = importlib.import_module(driver)
            handle = module.init_handler(config, logger)
            self._drivers[name] = handle

        values = config.get_values(Scheduler.section, Scheduler.params);
        for param in values:
            self.__dict__[param] = values[param]

    # TODO: move these functions to Logger class
    def _info(self, id_, message):
        self._logger.info('req #%s - %s' % (id_, message))

    def _warning(self, id_, message):
        self._logger.warning('req #%s - %s' % (id_, message))

    def _error(self, id_, message):
        self._logger.error('req #%s - %s' % (id_, message))

    def sync_backend_hosts(self, request):
        if not request.backend or not request.backend.hosts:
            return

        handle = self._drivers[request.scheduler.name]
        sched_hosts = handle.list_hosts();

        hosts = request.backend.hosts

        for h in hosts:
            for s in sched_hosts:
                if s['name'] in request.scheduler.hostnames:
                    continue

                if s['ip'] == h.ip and s['state'] == 'idle':
                    request.scheduler.append_hostname(s['name'])
                    break

    def remove_hosts(self, request):
        handle = self._drivers[request.scheduler.name]

        for h in request.scheduler.hostnames:
            handle.remove_host(h)
            request.scheduler.remove_hostname(h)

    def create_job(self, request):
        handle = self._drivers[request.scheduler.name]

        job = handle.Job(request.id_, request.template)
        job.set_resources(
            request.resources.nodes,
            request.resources.cpu,
            request.resources.memory,
            request.resources.time)
        job.set_hostnames(request.scheduler.hostnames)
        job.set_user_data(request.user_data)

        (request.job.localdir, request.job.remotedir) = handle.create_job(job)

    def transfer_job(self, request):
        handle = self._drivers[request.scheduler.name]
        handle.transfer_job(request.id_)

    def submit_job(self, request):
        handle = self._drivers[request.scheduler.name]
        i = handle.submit_job(request.job.remotedir)
        request.job.id_ = i

    def describe_job(self, request):
        handle = self._drivers[request.scheduler.name]
        job = handle.describe_job(request.job.id_)

        request.job.id_ = job.get('id', None)
        request.job.status = job.get('status', None)

    def stop_job(self, request):
        if not request.job.id_:
            return

        if request.job.status == 'done':
            return

        handle = self._drivers[request.scheduler.name]
        handle.stop_job(request.job.id_)
        request.job.status = 'done'

    def cleanup_job(self, request):
        handle = self._drivers[request.scheduler.name]

        handle.cleanup_local(request.job.localdir)
        request.job.localdir = None

        handle.cleanup_remote(request.job.remotedir)
        request.job.remotedir = None

