
import os
import sys
import shutil

path = os.path.abspath(__file__)
dir = os.path.dirname(path)
parentdir = os.path.dirname(os.path.dirname(dir))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.hooks import Hooks

class HooksTest(unittest.TestCase):
    cmd_succ = '/tmp/iu-test-succ.sh'
    out_succ = '/tmp/iu-test-succ-output'
    cmd_fail = '/tmp/iu-test-fail.sh'
    out_fail = '/tmp/iu-test-fail-output'
    local_root = 'tmp-local/'
    remote_root = 'tmp-remote/'
    command_template = '%(command)s'

    def setUp(self):
        self.logger = Mock()

        with open(self.cmd_succ, 'w') as f:
            f.write('#!/bin/bash\necho succ "$*" > %s\necho succ-out > /dev/stdout\necho succ-err > /dev/stderr\n' % self.out_succ)
        with open(self.cmd_fail, 'w') as f:
            f.write('#!/bin/bash\necho fail "$*" > %s\necho fail-out > /dev/stdout\necho fail-err > /dev/stderr\nexit 1\n' % self.out_fail)

        os.chmod(self.cmd_succ, 0o755)
        os.chmod(self.cmd_fail, 0o755)

        if not os.path.exists(self.local_root):
            os.makedirs(self.local_root)
        if not os.path.exists(self.remote_root):
            os.makedirs(self.remote_root)

        self.config = Mock()
        self.config.get_values.return_value = {
            'host': 'host',
            'command_template': self.command_template,
            'request_format': 'conf',
            'local_root': self.local_root,
            'remote_root': self.remote_root,
            'request_transfer': 'cp -r %(local_dir)s %(remote_dir)s'
        }
        
        self.hooks = Hooks(self.config, self.logger)

        self.request_dict = {
            'status': 'executing',
            'resources': {
                'nodes': 1,
                'cpu': 2,
                'memory': 3,
                'time': 3
            },
            'backend': {
                'name': 'b',
                'hosts': [
                    {'id': 1, 'ip': '10.10.0.1'}
                ],
            },
            'scheduler': {
                'name': 's',
                'hostnames': ['a']
            },
            'user_data': {
                'data_str': 'str1'
            },
            'job': {
                'id': '42',
                'status': 'done',
                'localdir': '/tmp/42.out',
                'remotedir': '/tmp/42.err',
            },
            'template': 'line 1\nline 2\n'
        }

    def tearDown(self):
        os.remove(self.cmd_succ)
        if os.path.exists(self.out_succ):
            os.remove(self.out_succ)

        os.remove(self.cmd_fail)
        if os.path.exists(self.out_fail):
            os.remove(self.out_fail)

        del self.hooks

        shutil.rmtree(self.local_root)
        shutil.rmtree(self.remote_root)

    def read_output(self, path):
        ret = '(none)'
        if not os.path.exists(path):
            return ret

        with open(path) as f:
            ret = f.read()

        return ret

    def test__ctor__called__params_are_requestd(self):
        args, kwargs = self.config.get_values.call_args 
        section, requested = args

        self.assertEqual(section, self.hooks.section)

        for p in requested:
            self.assertIn(p, self.hooks.params)

    def test__exec__no_hooks__returns(self):
        req = Mock()
        req.id_ = 1 
        req.hooks = []

        self.hooks.exec(req, 'executing', 'after')

        self.assertEqual(self.read_output(self.out_succ), '(none)')
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def _init_empty_hook(self, hook):
        hook.id_ = None
        hook.name = None
        hook.stage = None
        hook.condition = None
        hook.command = None
        hook.format = None
        hook.remote_dir = None
        hook.remote_root = None
        hook.remote_request_path = None
        hook.command_template = None
        hook.request_transfer = None
        hook.host = None
        hook.return_code = None
        hook.stdout = None
        hook.stderr = None
        hook.repeat = None

    def test__exec__not_match__not_exectued(self):
        req = Mock()
        req.id_ = 1 
        req.hooks = [Mock()]
        req.hooks[0].name = 'hook1'
        req.hooks[0].stage = 'submitting'
        req.hooks[0].command = self.cmd_succ

        self.hooks.exec(req, 'executing', 'after')

        self.assertEqual(self.read_output(self.out_succ), '(none)')
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def test__exec__any_stage__exectued(self):
        req = Mock()
        req.id_ = 1 
        req.hooks = [Mock()]

        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook1'
        req.hooks[0].stage = 'any'
        req.hooks[0].condition = 'ololo'
        req.hooks[0].command = self.cmd_succ

        self.hooks.exec(req, 'atata', 'ololo')

        self.assertEqual(req.hooks[0].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[0].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[0].return_code, 0)

        self.assertNotEqual(self.logger.info.call_args_list, [])

        self.assertEqual(self.read_output(self.out_succ), 'succ \n')
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def test__exec__custom_condition__exectued(self):
        req = Mock()
        req.id_ = 1 
        req.hooks = [Mock()]
        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook1'
        req.hooks[0].stage = 'atata'
        req.hooks[0].condition = 'ololo'
        req.hooks[0].command = self.cmd_succ

        self.hooks.exec(req, 'atata', 'ololo')

        self.assertEqual(req.hooks[0].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[0].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[0].return_code, 0)

        self.assertNotEqual(self.logger.info.call_args_list, [])

        self.assertEqual(self.read_output(self.out_succ), 'succ \n')
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def test__exec__conf_request_file(self):
        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock(), Mock()]
        self._init_empty_hook(req.hooks[0])
        self._init_empty_hook(req.hooks[1])
        req.hooks[0].id_ = 0
        req.hooks[0].stage = 'submitting'
        req.hooks[0].command = self.cmd_succ
        req.hooks[1].id_ = 1
        req.hooks[1].stage = 'executing'
        req.hooks[1].command = self.cmd_succ + ' arg'

        self.hooks.exec(req, 'executing', 'after'),

        self.assertEqual(req.hooks[1].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[1].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[1].return_code, 0)
        self.assertEqual(req.hooks[0].stdout, None)
        self.assertEqual(req.hooks[0].stderr, None)
        self.assertEqual(req.hooks[0].return_code, None)

        self.assertEqual(self.read_output(self.out_succ), 'succ arg\n')
        self.assertEqual(self.read_output(self.out_fail), '(none)')

        expected_request = [
            'hosts__0__ip="10.10.0.1"\n',
            'hosts__0__hostname="h1"\n',
            'hosts__0__id=1\n',
            'user_data__data_str="str1"\n',
            'job__error="/tmp/42.err"\n',
            'job__output="/tmp/42.out"\n',
            'job__id="42"\n',
            'job__status="done"\n',
            'status="executing"\n',
            'cpu=4\n',
            'hooks__0__stage="sumbitting"\n',
            'hooks__0__command="/tmp/iu-test-succ.sh"\n',
            'hooks__1__stage="executing"\n',
            'hooks__1__command="/tmp/iu-test-succ.sh arg"\n',
            'memory=4096\n',
            'host_count=1\n',
            'template="line 1\\nline 2\\n"\n',
        ]

        with open(self.remote_root + '42/request.conf') as f:
            for line in f:
                self.assertIn(line, expected_request)


    def test__transfer__default_values(self):
        cmd = self.cmd_succ
        cmd += ' %(request_id)s'
        cmd += ' %(hook_id)s'
        cmd += ' %(name)s'
        cmd += ' %(format)s'
        cmd += ' %(request_filename)s'
        cmd += ' %(local_root)s'
        cmd += ' %(local_dir)s'
        cmd += ' %(local_request_path)s'
        cmd += ' %(remote_root)s'
        cmd += ' %(remote_dir)s'
        cmd += ' %(remote_request_path)s'
        cmd += ' %(host)s'

        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock()]
        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].stage = 'executing'
        req.hooks[0].command = 'true'
        req.hooks[0].request_transfer = cmd

        self.hooks.request_transfer = cmd

        self.hooks.exec(req, 'executing', 'after'),

        args = self.read_output(self.out_succ)

        expected_args = 'succ'
        expected_args += ' 42'
        expected_args += ' 0'
        expected_args += ' hook_0'
        expected_args += ' conf'
        expected_args += ' request.conf'
        expected_args += ' tmp-local/'
        expected_args += ' tmp-local/42'
        expected_args += ' tmp-local/42/request.conf'
        expected_args += ' tmp-remote/'
        expected_args += ' tmp-remote/42'
        expected_args += ' tmp-remote/42/request.conf'
        expected_args += ' host'
        expected_args += '\n'

        self.assertEqual(args, expected_args)
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def test__transfer__command_overriden_values(self):
        cmd = self.cmd_succ
        cmd += ' %(request_id)s'
        cmd += ' %(hook_id)s'
        cmd += ' %(name)s'
        cmd += ' %(format)s'
        cmd += ' %(request_filename)s'
        cmd += ' %(local_root)s'
        cmd += ' %(local_dir)s'
        cmd += ' %(local_request_path)s'
        cmd += ' %(remote_root)s'
        cmd += ' %(remote_dir)s'
        cmd += ' %(remote_request_path)s'
        cmd += ' %(host)s'

        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock()]
        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook_name'
        req.hooks[0].stage = 'executing'
        req.hooks[0].command = 'true'
        req.hooks[0].request_transfer = cmd
        req.hooks[0].format = 'conf'
        req.hooks[0].remote_root = self.remote_root + '//'
        req.hooks[0].remote_dir = self.remote_root + '//42//'
        req.hooks[0].remote_request_path = self.remote_root + '//42//request.conf'
        req.hooks[0].host = 'host1'

        self.hooks.request_format = 'ini'
        self.hooks.remove_dir = ''

        self.hooks.exec(req, 'executing', 'after'),

        args = self.read_output(self.out_succ)

        expected_args = 'succ'
        expected_args += ' 42'
        expected_args += ' 0'
        expected_args += ' hook_name'
        expected_args += ' conf'
        expected_args += ' request.conf'
        expected_args += ' tmp-local/'
        expected_args += ' tmp-local/42'
        expected_args += ' tmp-local/42/request.conf'
        expected_args += ' tmp-remote///'
        expected_args += ' tmp-remote///42//'
        expected_args += ' tmp-remote///42//request.conf'
        expected_args += ' host1'
        expected_args += '\n'

        self.assertEqual(args, expected_args)
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def test__exec__command_default_values(self):
        cmd = self.cmd_succ
        cmd += ' %(request_id)s'
        cmd += ' %(hook_id)s'
        cmd += ' %(name)s'
        cmd += ' %(format)s'
        cmd += ' %(request_filename)s'
        cmd += ' %(local_root)s'
        cmd += ' %(local_dir)s'
        cmd += ' %(local_request_path)s'
        cmd += ' %(remote_root)s'
        cmd += ' %(remote_dir)s'
        cmd += ' %(remote_request_path)s'
        cmd += ' %(host)s'

        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock()]
        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].stage = 'executing'
        req.hooks[0].command = cmd

        self.hooks.exec(req, 'executing', 'after'),

        expected_args = 'succ'
        expected_args += ' 42'
        expected_args += ' 0'
        expected_args += ' hook_0'
        expected_args += ' conf'
        expected_args += ' request.conf'
        expected_args += ' tmp-local/'
        expected_args += ' tmp-local/42'
        expected_args += ' tmp-local/42/request.conf'
        expected_args += ' tmp-remote/'
        expected_args += ' tmp-remote/42'
        expected_args += ' tmp-remote/42/request.conf'
        expected_args += ' host'
        expected_args += '\n'

        self.assertEqual(self.read_output(self.out_succ), expected_args)
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def test__exec__command_overriden_values(self):
        cmd = self.cmd_succ
        cmd += ' %(request_id)s'
        cmd += ' %(hook_id)s'
        cmd += ' %(name)s'
        cmd += ' %(format)s'
        cmd += ' %(request_filename)s'
        cmd += ' %(local_root)s'
        cmd += ' %(local_dir)s'
        cmd += ' %(local_request_path)s'
        cmd += ' %(remote_root)s'
        cmd += ' %(remote_dir)s'
        cmd += ' %(remote_request_path)s'
        cmd += ' %(host)s'

        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock()]
        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook_name'
        req.hooks[0].stage = 'executing'
        req.hooks[0].condition = 'before'
        req.hooks[0].command = cmd
        req.hooks[0].command_template = '%(command)s tmpl'
        req.hooks[0].format = 'conf'
        req.hooks[0].remote_root = self.remote_root + '//'
        req.hooks[0].remote_dir = self.remote_root + '//42//'
        req.hooks[0].remote_request_path = self.remote_root + '//42//request.conf'
        req.hooks[0].host = 'host1'


        self.hooks.request_format = 'ini'
        self.hooks.remove_dir = ''

        self.hooks.exec(req, 'executing', 'before'),

        args = self.read_output(self.out_succ)

        expected_args = 'succ'
        expected_args += ' 42'
        expected_args += ' 0'
        expected_args += ' hook_name'
        expected_args += ' conf'
        expected_args += ' request.conf'
        expected_args += ' tmp-local/'
        expected_args += ' tmp-local/42'
        expected_args += ' tmp-local/42/request.conf'
        expected_args += ' tmp-remote///'
        expected_args += ' tmp-remote///42//'
        expected_args += ' tmp-remote///42//request.conf'
        expected_args += ' host1'
        expected_args += ' tmpl'
        expected_args += '\n'

        self.assertEqual(self.read_output(self.out_succ), expected_args)
        self.assertEqual(self.read_output(self.out_fail), '(none)')

    def test__exec__unsupported_format__throws(self):
        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock()]
        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].format = 'ini'
        req.hooks[0].stage = 'submitting'
        req.hooks[0].command = self.cmd_succ

        with self.assertRaises(Exception):
            self.hooks.exec(self.request, 'submitting', 'after')

    def test__exec__multiple_scripts__scripts_executed(self):
        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock(), Mock()]
        self._init_empty_hook(req.hooks[0])
        self._init_empty_hook(req.hooks[1])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook1'
        req.hooks[0].stage = 'submitting'
        req.hooks[0].command = self.cmd_succ + ' arg1'
        req.hooks[1].id_ = 1
        req.hooks[1].name = 'hook2'
        req.hooks[1].stage = 'submitting'
        req.hooks[1].command = self.cmd_succ + ' arg2'

        self.hooks.exec(req, 'submitting', 'after'),

        self.assertEqual(self.read_output(self.out_succ), 'succ arg2\n')
        self.assertEqual(self.read_output(self.out_fail), '(none)')

        self.assertEqual(req.hooks[0].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[0].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[0].return_code, 0)
        self.assertEqual(req.hooks[1].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[1].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[1].return_code, 0)

    def test__exec__hook_fails(self):
        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock(), Mock()]
        self._init_empty_hook(req.hooks[0])
        self._init_empty_hook(req.hooks[1])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook1'
        req.hooks[0].stage = 'submitting'
        req.hooks[0].command = self.cmd_fail
        req.hooks[1].id_ = 1
        req.hooks[1].name = 'hook2'
        req.hooks[1].stage = 'submitting'
        req.hooks[1].command = self.cmd_succ

        self.hooks.exec(req, 'submitting', 'after')

        self.assertNotEqual(self.logger.warning.call_args_list, [])

        self.assertEqual(self.read_output(self.out_fail), 'fail \n')
        self.assertEqual(self.read_output(self.out_succ), 'succ \n')

        self.assertEqual(req.hooks[0].stdout, 'fail-out\n')
        self.assertEqual(req.hooks[0].stderr, 'fail-err\n')
        self.assertEqual(req.hooks[0].return_code, 1)
        self.assertEqual(req.hooks[1].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[1].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[1].return_code, 0)

    def test__exec__hook_alreeady_executed__skipped(self):
        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock(), Mock()]
        self._init_empty_hook(req.hooks[0])
        self._init_empty_hook(req.hooks[1])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook1'
        req.hooks[0].stage = 'submitting'
        req.hooks[0].command = self.cmd_fail
        req.hooks[0].return_code = 10
        req.hooks[0].stdout = 'fail-out'
        req.hooks[0].stderr = 'fail-err'
        req.hooks[1].id_ = 1
        req.hooks[1].name = 'hook2'
        req.hooks[1].stage = 'submitting'
        req.hooks[1].command = self.cmd_succ

        self.hooks.exec(req, 'submitting', 'after')

        self.assertEqual(self.read_output(self.out_fail), '(none)')
        self.assertEqual(self.read_output(self.out_succ), 'succ \n')

        self.assertEqual(req.hooks[0].stdout, 'fail-out')
        self.assertEqual(req.hooks[0].stderr, 'fail-err')
        self.assertEqual(req.hooks[0].return_code, 10)
        self.assertEqual(req.hooks[1].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[1].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[1].return_code, 0)

    def test__exec__repeat_hook__executed_again(self):
        req = Mock()
        req.id_ = 42 
        req.hooks = [Mock()]
        self._init_empty_hook(req.hooks[0])
        req.hooks[0].id_ = 0
        req.hooks[0].name = 'hook1'
        req.hooks[0].stage = 'submitting'
        req.hooks[0].command = self.cmd_succ
        req.hooks[0].return_code = 30
        req.hooks[0].stdout = 'aa'
        req.hooks[0].stderr = 'bb'
        req.hooks[0].repeat = True 

        self.hooks.exec(req, 'submitting', 'after')

        self.assertEqual(self.read_output(self.out_fail), '(none)')
        self.assertEqual(self.read_output(self.out_succ), 'succ \n')

        self.assertEqual(req.hooks[0].stdout, 'succ-out\n')
        self.assertEqual(req.hooks[0].stderr, 'succ-err\n')
        self.assertEqual(req.hooks[0].return_code, 0)

if __name__ == '__main__':
    unittest.main()
