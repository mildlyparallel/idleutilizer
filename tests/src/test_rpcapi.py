import os
import sys

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.rpcapi import RPCApi

class RPCApiTest(unittest.TestCase):
    def setUp(self):
        self.config = Mock()

        self.logger = Mock()
        self.logger.warning = Mock()
        self.logger.error = Mock()
        self.logger.info = Mock()

        self.requests = Mock()

        self.api = RPCApi(self.config, self.logger, self.requests) 

    def tearDown(self):
        pass

    def test__requests_list__no_requests__returns_empty_list(self):
        self.requests.list.return_value = []
        self.assertEqual(self.api.requests_list(), [])

    def test__requests_list__throws__returns_error(self):
        self.requests.list.side_effect = Exception('str')
        output = self.api.requests_list()
        self.assertEqual(output['error'], 'str')

    def test__requests_list__haverequests__returns_empty_list(self):
        all = [ 
            (1, {
                'status': 'executing',
                'vm': 10, 'cpu': 4, 'memory': 4096,
                'owner': {'uid:': 1, 'gid': 2},
                'hosts': [
                    {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
                    {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
                ],
                'job': { 'status': 'done' }
            }),
            (2, {
                'status': 'executing',
                'vm': 10, 'cpu': 4, 'memory': 4096,
                'owner': {'uid:': 1, 'gid': 2},
                'hosts': [
                    {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
                    {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
                    {'id': 3, 'ip': '10.10.0.3', 'hostname': 'h3'},
                ],
                'job': { 'status': 'executing' }
            })
        ]

        self.requests.list.return_value = all

        self.assertEqual(self.api.requests_list(), [ 
            {
                'id': 1,
                'status': 'executing',
                'vm': 10, 'cpu': 4, 'memory': 4096,
                'owner': {'uid:': 1, 'gid': 2},
                'hosts': [
                    {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
                    {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
                ],
                'job': { 'status': 'done' }
            }, {
                'id': 2,
                'status': 'executing',
                'vm': 10, 'cpu': 4, 'memory': 4096,
                'owner': {'uid:': 1, 'gid': 2},
                'hosts': [
                    {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
                    {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
                    {'id': 3, 'ip': '10.10.0.3', 'hostname': 'h3'},
                ],
                'job': { 'status': 'executing' }
            }
        ])


    def test__requests_find__no_id__returns_error(self):
        output = self.api.requests_find(None)
        self.assertEqual(self.requests.find.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_find__wrong_type__returns_error(self):
        output = self.api.requests_find('1')
        self.assertEqual(self.requests.find.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_find__wrong_id__returns_error(self):
        output = self.api.requests_find(-1)
        self.assertEqual(self.requests.find.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_find__throws__returns_error(self):
        self.requests.find.side_effect = Exception('str')
        output = self.api.requests_find(1)
        self.assertEqual(output['error'], 'str')

    def test__requests_find__not_found__returns_empty_dict(self):
        self.requests.find.return_value = None
        output = self.api.requests_find(10)
        self.requests.find.assert_called_with(10)
        self.assertTrue('error' in output)

    def test__requests_find__found__returns_requset(self):
        self.requests.find.return_value = {
            'status': 'executing',
            'vm': 10, 'cpu': 4, 'memory': 4096,
            'owner': {'uid:': 1, 'gid': 2},
            'hosts': [
                {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
                {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
                {'id': 3, 'ip': '10.10.0.3', 'hostname': 'h3'},
            ],
            'job': { 'status': 'executing' }
        }

        self.assertEqual(self.api.requests_find(10), {
            'id': 10,
            'status': 'executing',
            'vm': 10, 'cpu': 4, 'memory': 4096,
            'owner': {'uid:': 1, 'gid': 2},
            'hosts': [
                {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
                {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
                {'id': 3, 'ip': '10.10.0.3', 'hostname': 'h3'},
            ],
            'job': { 'status': 'executing' }
        })

        self.requests.find.assert_called_with(10)

    def test__sumbit__no_req__returns_empty_dict(self):
        output = self.api.requests_submit(None)
        self.assertTrue('error' in output)
        self.assertEqual(self.requests.find.call_args_list, [])

    def test__sumbit__not_object__returns_empty_dict(self):
        output = self.api.requests_submit('str')
        self.assertTrue('error' in output)
        self.assertEqual(self.requests.find.call_args_list, [])

    def test__sumbit__throws__returns_error(self):
        self.requests.submit.side_effect = Exception('str')
        output = self.api.requests_submit({'a': 1})
        self.requests.submit.assert_called_with({'a': 1})
        self.assertEqual(output['error'], 'str')

    def test__sumbit__fails__returns_empty_dict(self):
        self.requests.submit.return_value = None
        output = self.api.requests_submit({'a': 1})
        self.requests.submit.assert_called_with({'a': 1})
        self.assertTrue('error' in output)

    def test__sumbit__succeed__returns_id(self):
        self.requests.submit.return_value = 10
        self.assertEqual(self.api.requests_submit({'a': 1}), 10)
        self.requests.submit.assert_called_with({'a': 1})

    def test__requests_stop__no_id__returns_error(self):
        output = self.api.requests_stop(None)
        self.assertEqual(self.requests.stop.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_stop__wrong_type__returns_error(self):
        output = self.api.requests_stop('1')
        self.assertEqual(self.requests.stop.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_stop__wrong_id__returns_error(self):
        output = self.api.requests_stop(-1)
        self.assertEqual(self.requests.stop.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_stop__throws__returns_error(self):
        self.requests.stop.side_effect = Exception('str')
        output = self.api.requests_stop(1)
        self.assertEqual(output['error'], 'str')

    def test__requests_stop__succeed__returns_id(self):
        self.requests.stop.return_value = 10
        output = self.api.requests_stop(10)
        self.assertEqual(output, 10)
        self.requests.stop.assert_called_with(10)

    def test__requests_delete_no_id__returns_error(self):
        output = self.api.requests_delete(None)
        self.assertEqual(self.requests.delete.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_delete__wrong_type__returns_error(self):
        output = self.api.requests_delete('1')
        self.assertEqual(self.requests.delete.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_delete__wrong_id__returns_error(self):
        output = self.api.requests_delete(-1)
        self.assertEqual(self.requests.delete.call_args_list, [])
        self.assertTrue('error' in output)

    def test__requests_delete__throws__returns_error(self):
        self.requests.delete.side_effect = Exception('str')
        output = self.api.requests_delete(1)
        self.assertEqual(output['error'], 'str')

    def test__requests_delete__succeed__returns_id(self):
        self.requests.delete.return_value = 10
        output = self.api.requests_delete(10)
        self.assertEqual(output, 10)
        self.requests.delete.assert_called_with(10)


if __name__ == '__main__':
    unittest.main()
