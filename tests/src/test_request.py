import os
import sys
import random

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.request import Request

def rnd_string():
    return ''.join(random.choice('qwertyuiop') for _ in range(4))


class RequestsTests(unittest.TestCase):
    default_nodes = 10
    default_cpu = 4
    default_memory = 4096
    default_time = 100
    retry_on_error = False
    transfer_to_owner = True

    def setUp(self):
        self.storage = Mock()
        self.logger = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'default_nodes': self.default_nodes,
            'default_cpu': self.default_cpu,
            'default_memory': self.default_memory,
            'default_time': self.default_time,
        }

        Request.set_config(self.config)
        Request.set_logger(self.logger)
        Request.set_storage(self.storage)
        
    def tearDown(self):
        pass

    def test__ctor__no_params__defaults_are_set(self):
        self.storage.create.return_value = 42

        req = Request({})

        self.assertEqual(req.id_, 42)
        self.assertEqual(req.status, 'pending')
        self.assertEqual(req.hooks, [])
        self.assertEqual(req.resources.nodes, self.default_nodes)
        self.assertEqual(req.resources.cpu, self.default_cpu)
        self.assertEqual(req.resources.memory, self.default_memory)
        self.assertEqual(req.resources.time, self.default_time)
        self.assertEqual(req.backend.name, None)
        self.assertEqual(req.backend.owner, None)
        self.assertEqual(req.backend.hosts, [])
        self.assertEqual(req.scheduler.name, None)
        self.assertEqual(req.scheduler.hostnames, [])
        self.assertEqual(req.job.id_, None)
        self.assertEqual(req.job.status, None)
        self.assertEqual(req.job.localdir, None)
        self.assertEqual(req.job.remotedir, None)
        self.assertEqual(req.user_data, {})
        self.assertEqual(req.template, None)
        self.assertEqual(req.error, None)
        self.assertEqual(req.delete_when_done, None)
        self.assertTrue(req.created_at > 10)
        self.assertTrue(req.status_updated_at > 10)

    def test__ctor__with_params__params_are_set(self):
        self.storage.create.return_value = 42

        req = Request({
            'status': 'executing',
            'created_at': 10,
            'status_updated_at': 11,
            'resources': { 'nodes': 1, 'cpu': 2, 'memory': 3, 'time': 4 },
            'hooks': [
                {
                    'command': 'c1',
                },
                {
                    'name': 'h2',
                    'stage': 's',
                    'command': 'c',
                    'request_transfer': 't',
                    'format': 'f',
                    'remote_root': 'r',
                    'remote_dir': 'd',
                    'remote_request_path': 'p',
                    'repeat': True,
                    'return_code': 10,
                    'host': 'h',
                    'condition': 'o',
                    'stderr': 'err',
                    'stdout': 'out',
                }
            ],
            'backend': {
                'name': 'b',
                'owner': {'a': 'b'},
                'hosts': [
                    {'id': 1, 'ip': '10.10.10.10', 'chown': True},
                    {'id': 2, 'ip': '10.10.10.12'},
                ]
            },
            'scheduler': {
                'name': 's',
                'hostnames': ['n1', 'n2']
            },
            'job': {
                'id': 10,
                'status': 's',
                'localdir': 'l',
                'remotedir': 'r',
            },
            'template': 't',
            'user_data': {'k': 'v'},
            'error': 'atata',
            'delete_when_done': True
        })

        self.assertEqual(req.id_, 42)
        self.assertEqual(req.status, 'executing')
        self.assertEqual(req.resources.nodes, 1)
        self.assertEqual(req.resources.cpu, 2)
        self.assertEqual(req.resources.memory, 3)
        self.assertEqual(req.resources.time, 4)
        self.assertEqual(req.hooks[0].id_, 0)
        self.assertEqual(req.hooks[0].name, None)
        self.assertEqual(req.hooks[0].command, 'c1')
        self.assertEqual(req.hooks[0].stage, None)
        self.assertEqual(req.hooks[0].condition, None)
        self.assertEqual(req.hooks[0].request_transfer, None)
        self.assertEqual(req.hooks[0].format, None)
        self.assertEqual(req.hooks[0].return_code, None)
        self.assertEqual(req.hooks[0].remote_root, None)
        self.assertEqual(req.hooks[0].repeat, None)
        self.assertEqual(req.hooks[0].remote_dir, None)
        self.assertEqual(req.hooks[0].remote_request_path, None)
        self.assertEqual(req.hooks[0].host, None)
        self.assertEqual(req.hooks[0].stdout, None)
        self.assertEqual(req.hooks[0].stderr, None)
        self.assertEqual(req.hooks[1].id_, 1)
        self.assertEqual(req.hooks[1].name, 'h2')
        self.assertEqual(req.hooks[1].stage, 's')
        self.assertEqual(req.hooks[1].command, 'c')
        self.assertEqual(req.hooks[1].condition, 'o')
        self.assertEqual(req.hooks[1].request_transfer, 't')
        self.assertEqual(req.hooks[1].format, 'f')
        self.assertEqual(req.hooks[1].remote_root, 'r')
        self.assertEqual(req.hooks[1].repeat, True)
        self.assertEqual(req.hooks[1].return_code, 10)
        self.assertEqual(req.hooks[1].remote_dir, 'd')
        self.assertEqual(req.hooks[1].remote_request_path, 'p')
        self.assertEqual(req.hooks[1].host, 'h')
        self.assertEqual(req.hooks[1].stdout, 'out')
        self.assertEqual(req.hooks[1].stderr, 'err')
        self.assertEqual(req.backend.name, 'b')
        self.assertEqual(req.backend.owner, {'a': 'b'})
        self.assertEqual(req.backend.hosts[0].id_, 1)
        self.assertEqual(req.backend.hosts[0].ip, '10.10.10.10')
        self.assertEqual(req.backend.hosts[0].chown, True)
        self.assertEqual(req.backend.hosts[1].id_, 2)
        self.assertEqual(req.backend.hosts[1].ip, '10.10.10.12')
        self.assertEqual(req.backend.hosts[1].chown, None)
        self.assertEqual(req.scheduler.name, 's')
        self.assertEqual(req.scheduler.hostnames, ['n1', 'n2'])
        self.assertEqual(req.job.id_, 10)
        self.assertEqual(req.job.status, 's')
        self.assertEqual(req.job.localdir, 'l')
        self.assertEqual(req.job.remotedir, 'r')
        self.assertEqual(req.user_data, {'k': 'v'})
        self.assertEqual(req.template, 't')
        self.assertEqual(req.error, 'atata')
        self.assertEqual(req.delete_when_done, True)
        self.assertEqual(req.created_at, 10)
        self.assertEqual(req.status_updated_at, 11)

    def assert_update(self, req, kk):
        v = rnd_string()
        k = kk.split('.')[-1]
        setattr(req, k, v)
        self.assertEqual(getattr(req, k), v)
        i, fields = self.storage.update.call_args[0]
        self.assertEqual(i, 42)

        f = fields
        for i in kk.split('.'):
            f = f[i]

        self.assertEqual(f, v)

    def test__ctor__updates__request_is_saved(self):
        self.storage.create.return_value = 42

        req = Request({})

        self.assert_update(req, 'status')
        self.assert_update(req, 'template')
        self.assert_update(req, 'error')
        self.assert_update(req, 'delete_when_done')

        self.assert_update(req.job, 'job.status')
        self.assert_update(req.job, 'job.localdir')
        self.assert_update(req.job, 'job.remotedir')
        self.assert_update(req.job, 'job.remotedir')

        req.backend.append_host(10)

        i, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['backend']['hosts'][0]['id'], 10)
        self.assertEqual(req.backend.hosts[0].id_, 10)

        req.backend.hosts[0].ip = '10'
        i, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['backend']['hosts'][0]['ip'], '10')
        self.assertEqual(req.backend.hosts[0].ip, '10')

        req.backend.hosts[0].chown = True
        i, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['backend']['hosts'][0]['chown'], True)
        self.assertEqual(req.backend.hosts[0].chown, True)

        req.scheduler.append_hostname('a1')
        i, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['scheduler']['hostnames'], ['a1'])
        self.assertEqual(req.scheduler.hostnames, ['a1'])

if __name__ == '__main__':
    unittest.main()
