import os
import sys

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.backend import Backend

def init_handler(logger, config):
    pass

class BackendTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.config = Mock()
        self.config.get_items.return_value = [
            ('test', 'tests.src.test_backend')
        ]
        self.config.get_values.return_value = {
            'test': 'tests.src.test_backend'
        }

        self.handle = Mock()

        with patch('tests.src.test_backend.init_handler', Mock(return_value=self.handle)):
            self.backend = Backend(self.config, self.logger)

    def test__ctor__param_driver_specified__driver_is_loaded(self):
        mock = MagicMock(return_value=Mock())
        with patch('tests.src.test_backend.init_handler', mock):
            Backend(self.config, self.logger)

        mock.assert_called_with(self.config, self.logger)

    def test__ctor__unknown_driver__throws(self):
        config = Mock()
        config.get_items.return_value = [('unknown', 'unknown')]

        with self.assertRaises(Exception):
            Backend(config, self.logger)

    def test__spawn_host__no_backend(self):
        req = Mock()
        req.backend = None

        self.backend.spawn_hosts(req)


    def test__spawn_host__no_backend_name__status_submitting(self):
        req = Mock()
        req.backend.name = None

        self.backend.spawn_hosts(req)

    def test__spawn_host__no_hosts__hosts_are_requested(self):
        req = Mock()
        req.resources.nodes = 3
        req.resources.cpu = 4
        req.resources.memory = 5
        req.backend.name = 'test'
        req.backend.hosts = []


        self.handle.spawn_host.side_effect = [1, 2, 3]

        self.backend.spawn_hosts(req)

        for arg in self.handle.spawn_host.call_args_list:
            p, = arg[0]
            self.assertEqual(p['cpu'], req.resources.cpu)
            self.assertEqual(p['memory'], req.resources.memory)

        self.assertEqual(
            req.backend.append_host.call_args_list,
            [call(1), call(2), call(3)]
        )

    def test__spawn_host__partially_spawned__reamining_are_requested(self):
        req = Mock()
        req.resources.nodes = 4
        req.resources.cpu = 4
        req.resources.memory = 5
        req.backend.name = 'test'
        req.backend.hosts = [Mock(), Mock()]
        req.backend.hosts[0].id_ = 1
        req.backend.hosts[1].id_ = 2

        self.handle.spawn_host.side_effect = [3, 4]

        self.backend.spawn_hosts(req)

        for arg in self.handle.spawn_host.call_args_list:
            p, = arg[0]
            self.assertEqual(p['cpu'], req.resources.cpu)
            self.assertEqual(p['memory'], req.resources.memory)

        self.assertEqual(
            req.backend.append_host.call_args_list,
            [call(3), call(4)]
        )

    def test__transfer_hosts__no_owner__status_set_to_bootstrapping(self):
        req = Mock()
        req.backend.name = 'test'
        req.backend.owner = None
        req.backend.hosts = [Mock(), Mock()]
        req.backend.hosts[0].id_ = 1
        req.backend.hosts[1].id_ = 2
        req.backend.hosts[0].chown = None
        req.backend.hosts[1].chown = None

        self.backend.transfer_hosts(req)
        self.assertEqual(req.backend.hosts[0].chown, None)
        self.assertEqual(req.backend.hosts[1].chown, None)

    def test__transfer_hosts__valid_owner__status_set_to_bootstrapping(self):
        owner = {'uid': 1, 'gid': 2}
        req = Mock()
        req.backend.name = 'test'
        req.backend.owner = owner
        req.backend.hosts = [Mock(), Mock()]
        req.backend.hosts[0].id_ = 1
        req.backend.hosts[1].id_ = 2
        req.backend.hosts[0].chown = None
        req.backend.hosts[1].chown = None

        self.backend.transfer_hosts(req)
        self.assertEqual(req.backend.hosts[0].chown, True)
        self.assertEqual(req.backend.hosts[1].chown, True)

        self.assertEqual(
            self.handle.chown_host.call_args_list,
            [call(1, owner), call(2, owner)]
        )

    def test__transfer_hosts__partitially_transferred__status_set_to_bootstrapping(self):
        owner = {'uid': 1, 'gid': 2}
        req = Mock()
        req.backend.name = 'test'
        req.backend.owner = owner
        req.backend.hosts = [Mock(), Mock()]
        req.backend.hosts[0].id_ = 1
        req.backend.hosts[1].id_ = 2
        req.backend.hosts[0].chown = True
        req.backend.hosts[1].chown = None

        self.backend.transfer_hosts(req)
        self.assertEqual(req.backend.hosts[0].chown, True)
        self.assertEqual(req.backend.hosts[1].chown, True)

        self.assertEqual(
            self.handle.chown_host.call_args_list,
            [call(2, owner)]
        )

    def test__sync_addresses__some_avaliable__status_dont_change(self):
        req = Mock()
        req.backend.name = 'test'
        req.backend.hosts = [Mock(), Mock()]
        req.backend.hosts[0].id_ = 1
        req.backend.hosts[1].id_ = 2
        req.backend.hosts[0].ip = None
        req.backend.hosts[1].ip = None

        self.handle.get_host_ip.side_effect = [None, '2']
        self.handle.get_hosts.return_value = [
            {'id': 1},
            {'id': 2, 'ip': '2'}
        ]

        self.backend.sync_addresses(req)

        self.handle.get_hosts.assert_called_with()

        self.assertEqual(req.backend.hosts[0].ip, None)
        self.assertEqual(req.backend.hosts[1].ip, '2')

    def test__sync_addresses__partitial__ip_updated(self):
        req = Mock()
        req.backend.name = 'test'
        req.backend.hosts = [Mock(), Mock()]
        req.backend.hosts[0].id_ = 1
        req.backend.hosts[1].id_ = 2
        req.backend.hosts[0].ip = '1'
        req.backend.hosts[1].ip = None

        self.handle.get_hosts.return_value = [
            {'id': 1, 'ip': '1'},
            {'id': 2, 'ip': '2'}
        ]

        self.backend.sync_addresses(req)
        self.handle.get_hosts.assert_called_with()

        self.assertEqual(req.backend.hosts[0].ip, '1')
        self.assertEqual(req.backend.hosts[1].ip, '2')

    def test__terminate_hosts__have_hosts__removal_requested(self):
        req = Mock()
        req.backend.name = 'test'
        req.backend.hosts = [Mock(), Mock()]
        req.backend.hosts[0].id_ = 1
        req.backend.hosts[1].id_ = 2

        self.backend.terminate_hosts(req)

        self.assertEqual(
            self.handle.remove_host.call_args_list,
            [call(1), call(2)]
        )

if __name__ == '__main__':
    unittest.main()
