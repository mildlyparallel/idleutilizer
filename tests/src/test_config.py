import os
import sys

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

import unittest

from src.config import Config

from pprint import pprint as pp

class ConfigTest(unittest.TestCase):
    def setUp(self):
        self.path = 'config-test.txt'
        self.file = open(self.path, 'w')
        self.file.write('\n[test_section_0]\n')
        self.file.write('par_string=string value\n')
        self.file.write('par_int=42\n')
        self.file.write('par_bool=False\n')
        self.file.write("par_eval={'name': 'par', 'num': 42, 'array': [1, 'a']}\n")
        self.file.write('\n[test_section_2]\n')
        self.file.write('par_var=%(variable)s\n')
        self.file.close()

    def tearDown(self):
        os.remove(self.path)

    def test__get_values__file_not_readable__throws(self):
        with self.assertRaises(Exception):
            config = Config('unknown.txt')
            values = config.get_values('test_section_0', self.params)

    def test__get_values__no_section__throws(self):
        with self.assertRaises(Exception):
            config = Config(self.path)
            values = config.get_values('unknown', self.params)

    def test__get_values__no_parameters__return_empty(self):
        config = Config(self.path)
        values = config.get_values('test_section_0', [])
        self.assertEqual(values, {})

    def test__get_values__missing_required_param__throws(self):
        params = [{ 'name': 'undef', 'default': None, 'type': str }]

        with self.assertRaisesRegex(Exception, '(?=.*undef)(?=.*test_section_0).*'):
            config = Config(self.path)
            values = config.get_values('test_section_0', params)

    def test__get_values__missing_optional_param__sets_default(self):
        params = [{ 'name': 'undef', 'default': 'default_value', 'type': str }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(values['undef'], 'default_value')

    def test__get_values__param_set_and_optional__value_is_set(self):
        params = [{ 'name': 'par_string', 'default': 'default value', 'type': str }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(values['par_string'], 'string value')

    def test__get_value__type_int__value_casted_to_int(self):
        params = [{ 'name': 'par_int', 'default': None, 'type': int }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(values['par_int'], 42)

    def test__get_value__type_bool__value_casted_to_bool(self):
        params = [{ 'name': 'par_bool', 'default': None, 'type': bool }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(values['par_bool'], False)

    def test__get_value__type_int_from_default__value_casted_to_int(self):
        params = [{ 'name': 'undef', 'default': '42', 'type': int }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(values['undef'], 42)

    def test__get_value__type_eval__value_casted_to_object(self):
        params = [{ 'name': 'par_int', 'default': None, 'type': int }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(values['par_int'], 42)

    def test__get_value__type_int__value_casted_to_int(self):
        params = [{ 'name': 'par_eval', 'default': None, 'type': eval }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(
            values['par_eval'],
            {'name': 'par', 'num': 42, 'array': [1, 'a']}
        )

    def test__get_value__type_int_from_default__value_casted_to_int(self):
        params = [{
            'name': 'undef',
            'default': "{'name': 'par', 'num': 42, 'array': [1, 'a']}",
            'type': eval
        }]

        config = Config(self.path)
        values = config.get_values('test_section_0', params)

        self.assertEqual(
            values['undef'],
            {'name': 'par', 'num': 42, 'array': [1, 'a']}
        )

    def test__get_value__pattern_in_value__returns_raw_string(self):
        params = [{ 'name': 'par_var', 'default': None, 'type': str }]

        config = Config(self.path)
        values = config.get_values('test_section_2', params)

        self.assertEqual(values['par_var'], '%(variable)s')

    def test__get_value__type_eval__value_casted_to_object(self):
        params = [{ 'name': 'undef', 'default': '%(variable)s', 'type': str }]

        config = Config(self.path)
        values = config.get_values('test_section_2', params)

        self.assertEqual(values['undef'], '%(variable)s')

if __name__ == '__main__':
    unittest.main()
