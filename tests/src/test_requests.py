import os
import sys
from time import time

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.request import Request
from src.requests import Requests

class RequestsTests(unittest.TestCase):
    default_host_count = 10
    default_cpu = 4
    default_time = 4
    default_memory = 4096
    default_owner = {'uid': -1, 'gid': -1}
    retry_on_error = False
    transfer_to_owner = True

    def setUp(self):
        self.logger = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'default_nodes': self.default_host_count,
            'default_cpu': self.default_cpu,
            'default_memory': self.default_memory,
            'default_owner': self.default_owner,
            'default_time': self.default_time,
            'retry_on_error': self.retry_on_error,
            'transfer_to_owner': self.transfer_to_owner
        }
        
        self.backend = Mock()
        self.scheduler = Mock()
        self.storage = Mock()
        self.backend = Mock()
        self.scheduler = Mock()
        self.hooks = Mock()

        self.hooks.exec.return_value = []

        Request.set_storage(self.storage)
        Request.set_config(self.config)
        Request.set_logger(self.logger)

        self.requests = Requests(
            self.config, self.logger, self.storage,
            self.backend, self.scheduler, self.hooks)

    def tearDown(self):
        pass

    def call_update(self, now = 1000):
        with patch('src.requests.now', Mock(return_value = now)):
            self.requests.update()

    def test__submit__valid_request__id_from_db_is_returned(self):
        req = { 'resources': { 'nodes': 10, 'cpu': 4, 'memory':  4096 } }
        self.storage.create.return_value = 42

        id_ = self.requests.submit(req)

        self.assertEqual(id_, 42)

    def test__submit__valid_request__status_is_set_to_pending(self):
        req = { 'resources': { 'nodes': 10, 'cpu': 4, 'memory':  4096 } }
        self.storage.create.return_value = 42

        t = int(time())

        self.requests.submit(req)

        doc, = self.storage.create.call_args[0]

        self.assertEqual(doc['status'], 'pending')
        self.assertTrue(doc['created_at'] >= t)

    def test__list__empty__returns_empty_array(self):
        self.storage.list.return_value = []
        self.assertEqual(self.requests.list(), [])

    def test__list__not_empty__return_all_docs(self):
        all = [(1, {'a': 2}), (2, {'a': 3})]
        self.storage.list.return_value = all
        self.assertEqual(self.requests.list(), all)

    def test__find__valid_id__db_is_accessed(self):
        req = {'a': 1}
        self.storage.find.return_value = req

        self.assertEqual(self.storage.find(42), req)
        self.storage.find.assert_called_with(42)

    def test__find__id_not_found__returns_None(self):
        self.storage.find.return_value = None
        self.assertEqual(self.storage.find(42), None)
        self.storage.find.assert_called_with(42)

    def test__update__empty_db__docs_are_requested(self):
        self.storage.list.return_value = []
        self.requests.update()

    def test__update__pending__no_backend__preparing_stage(self):
        req = {
            'resources': { 'nodes': 10, 'cpu': 4, 'memory':  4096 },
            'status': 'pending'
        }

        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'preparing')

    def test__update__pending__with_backend_no_hosts__no_updated(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'pending',
            'backend': {
                'name': 'test'
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.backend.spawn_hosts.call_args_list, [])
        self.assertEqual(self.storage.update.call_args_list, [])

    def test__update__pending__with_backend_hosts__booting_stage(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'pending',
            'backend': {
                'name': 'test',
                'hosts': [{'id': 1}]
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.backend.spawn_hosts.call_args_list, [])

        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'booting')

    def test__update__booting__no_ip__not_changed(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'booting',
            'backend': {
                'name': 'test',
                'hosts': [{'id': 1, 'ip': '1'}, {'id': 2}]
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.backend.sync_addresses.call_args_list, [])
        self.assertEqual(self.storage.update.call_args_list, [])

    def test__update__booting__with_ip__transferring_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'booting',
            'backend': {
                'name': 'test',
                'hosts': [{'id': 1, 'ip': '1'}, {'id': 2, 'ip': '2'}]
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.backend.sync_addresses.call_args_list, [])

        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'transferring')

    def test__update__transferring__some_not_transferred__not_chaged(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'transferring',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2'}
                ]
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.backend.transfer_hosts.call_args_list, [])
        self.assertEqual(self.storage.update.call_args_list, [])

    def test__update__transferring__all_transferred__bootstrapping_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'transferring',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}
                ]
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.backend.transfer_hosts.call_args_list, [])
        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'bootstrapping')

    def test__update__bootstrapping__not_all_hostnames__not_chaged(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'bootstrapping',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': []
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.scheduler.sync_backend_hosts.call_args_list, [])
        self.assertEqual(self.storage.update.call_args_list, [])

    def test__update__bootstrapping__all_hostnames__preparing_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'bootstrapping',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': ['a']
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.scheduler.sync_backend_hosts.call_args_list, [])
        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'preparing')

    def test__update__preparing__copying_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'preparing',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': []
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.scheduler.create_job.call_args_list, [])
        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'copying')

    def test__update__copying__copying_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'copying',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': []
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.scheduler.transfer_job.call_args_list, [])
        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'submitting')

    def test__update__submitting__submitting_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'submitting',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': []
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.scheduler.submit_job.call_args_list, [])
        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'executing')

    def test__update__executing__job_not_changed__no_updates(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'executing',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': ['a']
            },
            'job': {
                'status': 'idle'
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertEqual(self.storage.update.call_args_list, [])

    def test__update__executing__job_is_done__terminating_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'executing',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': ['a']
            },
            'job': {
                'status': 'idle',
                'status': 'done'
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'terminating')

    def test__update__terminating__done_status(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'terminating',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': ['a']
            },
            'job': {
                'status': 'idle',
                'status': 'done'
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.scheduler.stop_job.call_args_list, [])
        self.assertNotEqual(self.scheduler.remove_hosts.call_args_list, [])
        self.assertNotEqual(self.backend.terminate_hosts.call_args_list, [])

        id_, fields = self.storage.update.call_args[0]
        self.assertEqual(fields['status'], 'done')

    def test__update__done__delete_is_not_set__no_updates(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'done',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'scheduler': {
                'name': 'test',
                'hostnames': ['a']
            },
            'job': {
                'status': 'idle',
                'status': 'done'
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertEqual(self.scheduler.cleanup_job.call_args_list, [])
        self.assertEqual(self.storage.update.call_args_list, [])

    def test__update__done__delete_is_set__request_deleted(self):
        req = {
            'resources': { 'nodes': 1, 'cpu': 4, 'memory':  4096 },
            'status': 'done',
            'backend': {
                'name': 'test',
                'hosts': [
                    {'id': 1, 'ip': '1', 'chown': True}, 
                    {'id': 2, 'ip': '2', 'chown': True}, 
                ]
            },
            'delete_when_done': True,
            'scheduler': {
                'name': 'test',
                'hostnames': ['a']
            },
            'job': {
                'status': 'idle',
                'status': 'done'
            }
        }
        self.storage.list.return_value = [ (42, req) ]

        self.requests.update()

        self.assertNotEqual(self.scheduler.cleanup_job.call_args_list, [])
        self.assertNotEqual(self.storage.delete.call_args_list, [])

    # def test__update__hookd__status_error__error_hook_called(self):
    #     req = {
    #         'status': 'executing',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'hosts': [ {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'}, ],
    #         'job': { 'id': 42, 'status': 'idle' },
    #         'hooks': { 'name': 'hook1' }
    #     }
    #
    #     self.storage.list.return_value = [ (1, req) ]
    #
    #     self.scheduler.describe_job.side_effect = Exception('str')
    #
    #     self.hooks.exec.side_effect = [
    #         Exception('str')
    #     ]
    #
    #     self.call_update()
    #
    #     id, fields = self.storage.update.call_args[0]
    #
    #     self.assertEqual(fields['error'], 'str')
    #
    #     calls = self.hooks.exec.call_args_list
    #     self.assertEqual(calls[0], call(1, req, 'executing', 'before'))
    #     new_req = req.copy()
    #     new_req['error'] = 'str'
    #     self.assertEqual(calls[1], call(1, new_req, 'executing', 'error'))
    #
    # def test__update__hooks__before_throws__error_is_set(self):
    #     req = { 
    #         'status': 'executing',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'hosts': [ {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'} ],
    #         'job': { 'id': 42, 'status': 'idle' },
    #         'hooks': { 'name': 'hook1' }
    #     }
    #
    #     self.storage.list.return_value = [ (1, req) ]
    #
    #     self.hooks.exec.side_effect = [
    #         Exception('str')
    #     ]
    #
    #     self.call_update()
    #
    #     id, fields = self.storage.update.call_args[0]
    #
    #     self.assertTrue('error' in fields)
    #
    #     calls = self.hooks.exec.call_args_list
    #     self.assertEqual(calls[0], call(1, req, 'executing', 'before'))
    #     new_req = req.copy()
    #     new_req['error'] = 'str'
    #     self.assertEqual(calls[1], call(1, new_req, 'executing', 'error'))
    #
    # def test__update__hooks__after_throws__error_is_set(self):
    #     req = {
    #         'status': 'terminating',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'job': { 'status': 'done' },
    #         'hooks': { 'name': 'hook1' }
    #     }
    #
    #     self.storage.list.return_value = [ (1, req) ]
    #
    #     self.hooks.exec.side_effect = [
    #         [{'name': 'hook2'}], 
    #         Exception('str')
    #     ]
    #
    #     self.scheduler.stop_job.return_value = {'id': 1}, None
    #     self.backend.terminate_hosts.return_value = [], None
    #
    #     self.call_update(100)
    #
    #     id, fields = self.storage.update.call_args[0]
    #
    #     new_req = req.copy()
    #     new_req['hooks'] = [{'name': 'hook2'}]
    #     new_req['job'] = {'id': 1}
    #     new_req['status'] = 'done'
    #     new_req['status_updated_at'] = 100
    #
    #     calls = self.hooks.exec.call_args_list
    #
    #     self.assertEqual(calls[0], call(1, req, 'terminating', 'before'))
    #     self.assertEqual(calls[1], call(1, new_req, 'terminating', 'after'))
    #
    #     new_req['error'] = 'str'
    #     self.assertEqual(calls[2], call(1, new_req, 'terminating', 'error'))
    #
    #     self.assertTrue('error' in fields)
    #     self.assertEqual(fields['hooks'], [{'name': 'hook2'}])
    #
    # def test__update__hooks__status_change__before_executed(self):
    #     req = {
    #         'status': 'executing',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'hosts': [ {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'} ],
    #         'job': { 'id': 42, 'status': 'idle' },
    #         'hooks': { 'name': 'hook1' }
    #     }
    #
    #     self.storage.list.return_value = [ (1, req) ]
    #
    #     self.scheduler.describe_job.return_value = { 'id': 42, 'status': 'running' }, None
    #
    #     self.hooks.exec.side_effect = [
    #         [{'name': 'hook2'}]
    #     ]
    #
    #     self.call_update()
    #
    #     id, fields = self.storage.update.call_args[0]
    #
    #     calls = self.hooks.exec.call_args_list
    #     self.assertEqual(calls, [call(1, req, 'executing', 'before')])
    #     self.assertEqual(fields['hooks'], [{'name': 'hook2'}])
    #
    # def test__update__hooks__status_change__before_and_after_executed(self):
    #     req = {
    #         'status': 'terminating',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'job': { 'status': 'done' },
    #         'hooks': [{ 'name': 'hook1' }, {'name': 'hook2'}]
    #     }
    #
    #     self.storage.list.return_value = [ (1, req) ]
    #
    #     self.hooks.exec.side_effect = [
    #         [{'name': 'hook1', 'rc': 0}, {'name': 'hook2'}], 
    #         [{'name': 'hook1', 'rc': 0}, {'name': 'hook2', 'rc': 1}], 
    #     ]
    #
    #     self.scheduler.stop_job.return_value = {'id': 1}, None
    #     self.backend.terminate_hosts.return_value = [], None
    #
    #     self.call_update(100)
    #
    #     id, fields = self.storage.update.call_args[0]
    #
    #     new_req = req.copy()
    #     new_req['hooks'] = [
    #         {'name': 'hook1', 'rc': 0}, {'name': 'hook2'}
    #     ]
    #     new_req['status'] = 'done'
    #     new_req['job'] = {'id': 1}
    #     new_req['status_updated_at'] = 100
    #
    #     calls = self.hooks.exec.call_args_list
    #
    #     self.assertEqual(calls[0], call(1, req, 'terminating', 'before'))
    #     self.assertEqual(calls[1], call(1, new_req, 'terminating', 'after'))
    #
    #     self.assertEqual(fields['hooks'], [{'name': 'hook1', 'rc': 0}, {'name': 'hook2', 'rc': 1}])
    #
    # def test__delete__no_req__ignored(self):
    #     self.storage.find.return_value = None
    #
    #     self.requests.delete(1)
    #
    #     self.storage.find.assert_called_with(1)
    #
    #     self.assertEqual(self.storage.delete.call_args_list, [])
    #     self.assertEqual(self.storage.update.call_args_list, [])
    #
    # def test__delete__delete_is_set(self):
    #     self.storage.find.return_value =  {
    #         'status': 'terminating',
    #         'host_count': 1, 'cpu': 4, 'memory': 4096,
    #         'hosts': [ {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'}, ],
    #         'job': { 'id': 42, 'status': 'done' }
    #     }
    #
    #     self.requests.delete(1)
    #
    #     self.storage.find.assert_called_with(1)
    #
    #     self.assertEqual(self.storage.update.call_args_list, [call(1, {'delete_when_done': True})])
    #
    # def test__stop__no_req__ignored(self):
    #     self.storage.find.return_value = None
    #
    #     self.requests.stop(1)
    #
    #     self.storage.find.assert_called_with(1)
    #
    #     self.assertEqual(self.storage.update.call_args_list, [])
    #
    # def test__stop__terminating__ignored(self):
    #     self.storage.find.return_value =  {
    #         'status': 'terminating',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'hosts': [
    #             {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
    #             {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
    #             {'id': 3, 'ip': '10.10.0.3', 'hostname': 'h3'},
    #         ],
    #         'job': {
    #             'id': 42,
    #             'status': 'done'
    #         }
    #     }
    #
    #
    #     self.requests.stop(1)
    #
    #     self.storage.find.assert_called_with(1)
    #
    #     self.assertEqual(self.storage.update.call_args_list, [])
    #
    # def test__stop__done__ignored(self):
    #     self.storage.find.return_value = {
    #         'status': 'done',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'hosts': [ ],
    #         'job': {
    #             'id': 42,
    #             'status': 'done'
    #         }
    #     }
    #
    #     self.requests.stop(1)
    #
    #     self.storage.find.assert_called_with(1)
    #
    #     self.assertEqual(self.storage.update.call_args_list, [])
    #
    # def test__stop__any_status__status_is_set_terminating(self):
    #     self.storage.find.return_value = {
    #         'status': 'executing',
    #         'host_count': 10, 'cpu': 4, 'memory': 4096,
    #         'hosts': [
    #             {'id': 1, 'ip': '10.10.0.1', 'hostname': 'h1'},
    #             {'id': 2, 'ip': '10.10.0.2', 'hostname': 'h2'},
    #             {'id': 3, 'ip': '10.10.0.3', 'hostname': 'h3'},
    #         ],
    #         'job': {
    #             'id': 42,
    #             'status': 'running'
    #         }
    #     }
    #
    #     with patch('src.requests.now', Mock(return_value = 50)):
    #         self.requests.stop(1)
    #
    #     self.storage.find.assert_called_with(1)
    #
    #     id, fields = self.storage.update.call_args[0]
    #
    #     self.assertEqual(fields['status'], 'terminating')
    #     self.assertEqual(fields['status_updated_at'], 50)

if __name__ == '__main__':
    unittest.main()
