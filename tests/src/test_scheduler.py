import os
import sys

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.scheduler import Scheduler

def init_handler():
    pass

class SchedulerTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.config = Mock()
        self.config.get_items.return_value = [
            ('test', 'tests.src.test_scheduler')
        ]
        self.config.get_values.return_value = {
            'local_root': 'l',
            'remote_root': 'r'
        }

        self.handle = Mock()

        with patch('tests.src.test_scheduler.init_handler', Mock(return_value=self.handle)):
            self.scheduler = Scheduler(self.config, self.logger)

    def test__ctor__param_driver_specified__driver_is_loaded(self):
        mock = MagicMock(return_value=Mock())
        with patch('tests.src.test_scheduler.init_handler', mock):
            Scheduler(self.config, self.logger)

        mock.assert_called_with(self.config, self.logger)

    def test__ctor__unknown_driver__throws(self):
        config = Mock()
        config.get_items.return_value = [('unknown', 'unknown')]

        with self.assertRaises(Exception):
            Scheduler(config, self.logger)

    def test__sync_backend_hosts__no_backend__returns_empty(self):
        req = Mock()
        req.backend = None

        self.scheduler.sync_backend_hosts(req)

    def test__sync_backend_hosts__no_hosts__returns_empty(self):
        req = Mock()
        req.backend.hosts = []

        self.scheduler.sync_backend_hosts(req)

    def test__sync_backend_hosts__scheduler_hosts_dont_match__returns_empty(self):
        self.handle.list_hosts.return_value = [
            {'ip': '10.9.0.0', 'state': 'idle', 'name': 'n1'},
            {'ip': '10.9.0.1', 'state': 'idle', 'name': 'n1'}
        ]

        req = Mock()
        req.backend.hosts = [ Mock() ]
        req.backend.hosts[0].ip = '10.10.0.1'
        req.scheduler.name = 'test'
        req.scheduler.hostnames = []

        self.scheduler.sync_backend_hosts(req)

        req.scheduler.append_hostname.assert_not_called()

    def test__sync_backend_hosts__some_match__returns_names(self):
        self.handle.list_hosts.return_value = [
            {'ip': '10.10.0.1',  'state': 'idle',  'name': 'n1'},
            {'ip': '10.9.0.9',   'state': 'idle',  'name': 'n9'},
            {'ip': '10.10.0.3',  'state': 'idle',  'name': 'n2'},
        ]

        req = Mock()
        req.backend.hosts = [ Mock(), Mock(), Mock() ]
        req.backend.hosts[0].ip = '10.10.0.1'
        req.backend.hosts[1].ip = '10.10.0.2'
        req.backend.hosts[2].ip = '10.10.0.3'
        req.scheduler.name = 'test'
        req.scheduler.hostnames = []

        self.scheduler.sync_backend_hosts(req)

        self.assertEqual(
            req.scheduler.append_hostname.call_args_list,
            [call('n1'), call('n2')]
        )

    def test__sync_backend_hosts__not_idle__skipped(self):
        self.handle.list_hosts.return_value = [
            {'ip': '10.10.0.1', 'state': 'something', 'name': 'n1'},
            {'ip': '10.10.0.3', 'state': 'idle', 'name': 'n2'},
        ]

        req = Mock()
        req.backend.hosts = [ Mock(), Mock(), Mock() ]
        req.backend.hosts[0].ip = '10.10.0.1'
        req.backend.hosts[1].ip = '10.10.0.2'
        req.backend.hosts[2].ip = '10.10.0.3'
        req.scheduler.name = 'test'
        req.scheduler.hostnames = []

        self.scheduler.sync_backend_hosts(req)

        self.assertEqual(
            req.scheduler.append_hostname.call_args_list,
            [call('n2')]
        )

    def test__sync_backend_hosts__all_match__hostnames_reurned(self):
        self.handle.list_hosts.return_value = [
            {'ip': '10.10.0.2', 'state': 'idle', 'name': 'n2'},
            {'ip': '10.10.0.1', 'state': 'idle', 'name': 'n1'},
            {'ip': '10.10.0.3', 'state': 'idle', 'name': 'n3'},
            {'ip': '10.10.0.7', 'state': 'idle', 'name': 'n7'},
        ]

        req = Mock()
        req.backend.hosts = [ Mock(), Mock(), Mock() ]
        req.backend.hosts[0].ip = '10.10.0.1'
        req.backend.hosts[1].ip = '10.10.0.2'
        req.backend.hosts[2].ip = '10.10.0.3'
        req.scheduler.hostnames = []
        req.scheduler.name = 'test'

        self.scheduler.sync_backend_hosts(req)

        self.assertEqual(
            req.scheduler.append_hostname.call_args_list,
            [call('n1'), call('n2'), call('n3')]
        )

    def test__sync_backend_hosts__driver_throws__error_returned(self):
        self.handle.list_hosts.side_effect = Exception('str')

        req = Mock()
        req.backend.hosts = [ Mock(), Mock() ]
        req.backend.hosts[0].ip = '10.10.0.1'
        req.backend.hosts[1].ip = '10.10.0.2'
        req.scheduler.hostnames = []
        req.scheduler.name = 'test'

        with self.assertRaises(Exception):
            self.scheduler.sync_backend_hosts(req)
            hosts = self.scheduler.sync_backend_hosts('test', [
                '10.10.0.1', '10.10.0.2'
            ])

    def test__remove_hosts__valid_hostname__driver_accessed(self):
        req = Mock()
        req.scheduler.name = 'test'
        req.scheduler.hostnames = ['a1', 'a2']

        self.scheduler.remove_hosts(req)

        self.assertEqual(
            self.handle.remove_host.call_args_list, [call('a1'), call('a2')]
        )

    def test__remove_hosts__driver_throws__raises(self):
        req = Mock()
        req.scheduler.name = 'test'
        req.scheduler.hostnames = ['a1', 'a2']

        self.handle.remove_host.side_effect = Exception('s')

        with self.assertRaises(Exception):
            self.scheduler.remove_hosts(req)

    def test__create_job__driver_throws__error_returned(self):
        req = Mock()
        req.scheduler.name = 'test'
        req.scheduler.hostnames = ['a1', 'a2']

        self.handle.create_job.side_effect = Exception('str')

        with self.assertRaises(Exception):
            self.scheduler.create_job(req)

    def test__create_job__called__access_api(self):
        req = Mock()
        req.id_ = 42
        req.scheduler.name = 'test'
        req.scheduler.hostnames = ['a1', 'a2']

        self.handle.create_job.return_value = ('l/42', 'r/42')

        self.scheduler.create_job(req)

        self.handle.create_job.call_args[0]

        self.assertEqual(req.job.localdir, 'l/42')
        self.assertEqual(req.job.remotedir, 'r/42')

        # TODO: assert fields

    def test__transfer_job__called__access_api(self):
        req = Mock()
        req.id_ = 42
        req.scheduler.name = 'test'
        req.scheduler.hostnames = ['a1', 'a2']
        req.job.localdir = 'll'

        self.scheduler.transfer_job(req)

        req_id, = self.handle.transfer_job.call_args[0]

        self.assertEqual(req_id, 42)

    def test__transfer_job__driver_throws__error_returned(self):
        req = Mock()
        req.id_ = 42
        req.scheduler.name = 'test'
        req.scheduler.hostnames = ['a1', 'a2']
        req.job.localdir = 'll'

        self.handle.transfer_job.side_effect = Exception('str')

        with self.assertRaises(Exception):
            self.scheduler.transfer_job(req)

    def test__submit_job__called__access_api(self):
        req = Mock()
        req.id_ = 42
        req.scheduler.name = 'test'
        req.job.remotedir = 'rr'

        self.handle.submit_job.return_value = 10

        self.scheduler.submit_job(req)

        self.handle.submit_job.assert_called_with('rr')
        
        self.assertEqual(req.job.id_, 10)


    def test__describe_job__called__access_api(self):
        req = Mock()
        req.id_ = 42
        req.scheduler.name = 'test'
        req.job.id_ = 10

        job = {
            'id': 10,
            'status': 'running'
        }

        self.handle.describe_job.return_value = job

        self.scheduler.describe_job(req)

        self.handle.describe_job.assert_called_with(10)

        self.assertEqual(req.job.id_, 10)
        self.assertEqual(req.job.status, 'running')

    def test__stop_job__called__access_api(self):
        req = Mock()
        req.id_ = 42
        req.scheduler.name = 'test'
        req.job.id_ = 10

        self.scheduler.stop_job(req)

        self.handle.stop_job.assert_called_with(10)

        self.assertEqual(req.job.status, 'done')

    def test__cleanup_job__called__access_api(self):
        req = Mock()
        req.id_ = 42
        req.scheduler.name = 'test'
        req.job.id_ = 10
        req.job.localdir = 'l'
        req.job.remotedir = 'r'

        self.scheduler.cleanup_job(req)

        self.handle.cleanup_local.assert_called_with('l')
        self.handle.cleanup_remote.assert_called_with('r')

        self.assertEqual(req.job.localdir, None)
        self.assertEqual(req.job.remotedir, None)


if __name__ == '__main__':
    unittest.main()
