import os
import sys

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.requests_storage import RequestsStorage

class RequestsStorageTest(unittest.TestCase):
    db_path = 'test_db.json'

    def setUp(self):
        self.logger = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'db_path': self.db_path
        }
        
        self.storage = RequestsStorage(self.config, self.logger)

    def tearDown(self):
        del self.storage
        os.remove(self.db_path)

    def test__ctor__called__params_are_requestd(self):
        args, kwargs = self.config.get_values.call_args 
        section, requested = args

        self.assertEqual(section, self.storage.section)

        for p in requested:
            self.assertIn(p, self.storage.params)

    def test__create(self):
        o1 = {'id': 1, 'a': 'o1', 'b': [1, 2, 3]}
        self.storage.create(o1)

    def test__list__not_empty__returns_all_docs(self):
        o1 = {'id': 1, 'a': 'o1', 'b': [1, 2, 3]}
        o2 = {'id': 2, 'a': 'o2'}
        o3 = {'id': 3, 'a': 'o2', 'c': 'aa'}

        id1 = self.storage.create(o1)
        id2 = self.storage.create(o2)
        id3 = self.storage.create(o3)

        self.assertEqual(self.storage.list(), [
            (id1, o1),
            (id2, o2),
            (id3, o3)
        ])

    def test__list__empty__returns_empty_array(self):
        self.assertEqual(self.storage.list(), []) 

    def test__find_by_id__invalid__None_returned(self):
        self.assertEqual(self.storage.find(999), None)

    def test__find_by_id__valid__document_returned(self):
        o1 = {'id': 1, 'a': 'o1', 'b': [1, 2, 3]}
        o2 = {'id': 2, 'a': 'o2'}
        o3 = {'id': 3, 'a': 'o2', 'c': 'aa'}

        id1 = self.storage.create(o1)
        id2 = self.storage.create(o2)
        id3 = self.storage.create(o3)

        self.assertEqual(self.storage.find(id1), o1)
        self.assertEqual(self.storage.find(id2), o2)
        self.assertEqual(self.storage.find(id3), o3)

    def test__find__invalid__None_returned(self):
        self.assertEqual(self.storage.find({'id': 999}), [])

    def test__find__valid__document_returnred(self):
        o1 = {'id': 1, 'a': 'o1', 'b': [1, 2, 3]}
        o2 = {'id': 2, 'a': 'o2'}
        o3 = {'id': 2, 'a': 'o2', 'c': 'aa'}

        id1 = self.storage.create(o1)
        id2 = self.storage.create(o2)
        id3 = self.storage.create(o3)

        self.assertEqual(self.storage.find({'id': 1}), [(id1, o1)])
        self.assertEqual(self.storage.find({'id': 2}), [(id2, o2), (id3, o3)])

    def test__update__invalid_query__doc_not_modified(self):
        self.storage.update({'id': 999}, {'a': 10})
        self.assertEqual(self.storage.find({'id': 999}), [])

    def test__update_by_id__invalid_id__doc_not_modified(self):
        self.storage.update(999, {'a': 10})
        self.assertEqual(self.storage.find(999), None)

    def test__update__change_field__field_changed(self):
        o1 = {'id': 1, 'a': 'o1'}
        id = self.storage.create(o1)

        o2 = o1
        o2['a'] = 'o2'
        self.storage.update({'id': 1}, {'a': 'o2'})
        self.assertEqual(self.storage.find({'id': 1}), [(id, o2)])

    def test__update_by_id__change_field_by_id__field_changed(self):
        o1 = {'id': 1, 'a': 'o1'}
        id = self.storage.create(o1)

        o2 = o1
        o2['a'] = 'o2'
        self.storage.update(id, {'a': 'o2'})
        self.assertEqual(self.storage.find(id), o2)

    def test__update__add_field__fild_is_added(self):
        o1 = {'id': 1, 'a': 'o1'}
        id = self.storage.create(o1)

        o2 = o1
        o2['b'] = [3, 4, 5]
        self.storage.update({'id': 1}, o2)
        self.assertEqual(self.storage.find({'id': 1}), [(id, o1)])

    def test__delete__invalid_query(self):
        self.storage.delete({'id': '2'})

    def test__delete__invalid_id(self):
        self.storage.delete(2)

    def test__delete__valid__documents_are_deleted(self):
        o1 = {'id': 1, 'a': 'o1', 'b': [1, 2, 3]}
        o2 = {'id': 2, 'a': 'o2'}

        self.storage.create(o1)
        self.storage.create(o2)

        self.storage.delete({'id': 1})
        self.assertEqual(self.storage.find({'id': 1}), [])

        self.storage.delete({'a': 'o2'})
        self.assertEqual(self.storage.find({'id': 2}), [])

    def test__delete_by_id__valid__documents_are_deleted(self):
        o1 = {'id': 1, 'a': 'o1', 'b': [1, 2, 3]}
        o2 = {'id': 2, 'a': 'o2'}

        id1 = self.storage.create(o1)
        id2 = self.storage.create(o2)

        self.storage.delete(id1)
        self.assertEqual(self.storage.find(id1), None)

        self.storage.delete(id2)
        self.assertEqual(self.storage.find(id2), None)

    def test__concurrent_reads(self):
        # TODO: implement
        pass

    def test__concurrent_updates(self):
        # TODO: implement
        pass

    def test__concurrent_deletes(self):
        # TODO: implement
        pass

if __name__ == '__main__':
    unittest.main()
