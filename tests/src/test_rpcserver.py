import os
import sys

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from time import sleep

import xmlrpc.client
import xmltodict
from http.client import RemoteDisconnected 


from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from src.rpcserver import RPCServer

class ApiStub():
    def ret_string(self):
        return 'string'
        

class RPCServerTest(unittest.TestCase):
    endpoint = 'http://127.0.0.1:48989/'

    def setUp(self):
        self.logger = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'path': '/',
            'host': '127.0.0.1',
            'port': 48989,
            'whitelist': [],
            'log_requests': False
        }

        self.server = Mock()

    def tearDown(self):
        self.server.join()
        del self.server

    def test__ctor__called__params_are_requestd(self):
        self.server = RPCServer(self.config, self.logger)

        args, kwargs = self.config.get_values.call_args 
        section, requested = args

        self.assertEqual(section, self.server.section)

        for p in requested:
            self.assertIn(p, self.server.params)

    def call_api(self, method):
        try:
            proxy = xmlrpc.client.ServerProxy(self.endpoint)
            fn = getattr(proxy, method)
            return fn()
        except BrokenPipeError as e:
            sleep(1)
            self.call_api(method)

    def test__forked__accepts_api_reqests(self):
        self.server = RPCServer(self.config, self.logger)

        api = ApiStub()
        self.server.register_api(api)

        self.server.fork()

        ret = self.call_api('ret_string')
        self.assertEqual(ret, 'string')

    def test__ip_not_in_whitelist__not_connected(self):
        self.config.get_values.return_value['whitelist'] = ['10.10.10.10']

        self.server = RPCServer(self.config, self.logger)

        api = ApiStub()
        self.server.register_api(api)

        self.server.fork()

        with self.assertRaises(RemoteDisconnected):
            self.call_api('ret_string')

        self.server.join()

if __name__ == '__main__':
    unittest.main()
