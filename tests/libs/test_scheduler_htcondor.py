import os
import sys
import shutil

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from libs.schedulers.htcondor import HTCondor
from libs.schedulers.htcondor import init_handler

class HTCondorTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.config = Mock()
        self.config.get_values.return_value = { }

    def tearDown(self):
        del self.logger
        del self.config

    def test__init_handler__called__create_api_object(self):
        mock = Mock()
        with patch('libs.schedulers.htcondor.HTCondor', mock):
            init_handler(self.config, self.logger)
        mock.assert_called_with(self.config, self.logger)

    def test__ctor__called__params_are_requestd(self):
        scheduler = HTCondor(self.config, self.logger)

        args, kwargs = self.config.get_values.call_args 
        section, requested = args

        self.assertEqual(section, scheduler.section)

        for p in requested:
            self.assertIn(p, scheduler.params)

class HTCondorMethodTest(unittest.TestCase):
    local_dir = './local/'
    remote_dir = './remote/'
    template_path = 'job.tmpl'

    def setUp(self):
        self.logger = Mock()
        self.logger.error = Mock()
        self.logger.warning = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'host': 'localhost',
            'command_template': 'template %(host)s %(command)s',
            'command_remove_host': 'remove %(hostname)s',
            'command_list_hosts': 'list_hosts',
            'command_submit_job': 'submit %(remote_path)s',
            'command_stop_job': 'remove %(id)s',
            'command_describe_job': 'describe %(id)s',
            'command_copy': 'cp -r %(local_path)s %(remote_path)s',
            'local_root': self.local_dir,
            'remote_root': self.remote_dir,
            'default_template': self.template_path,
            'command_rm': 'rm %(path)s'
        }

        self.out = Mock()
        self.out.decode.return_value = '0'

        self.sp = Mock()
        self.sp.communicate.return_value = (self.out, 'err')
        self.sp.pid = 100
        self.sp.returncode = 0

        self.popen = Mock(return_value=self.sp)

        self.scheduler = HTCondor(self.config, self.logger)

        if not os.path.exists(self.local_dir):
            os.makedirs(self.local_dir)

        if not os.path.exists(self.remote_dir):
            os.makedirs(self.remote_dir)

        tmpl = '''$request_id
$root
$output
$error
$log
$nodes
$cpu
$memory
$requirements
str = $user_data_str
int = $user_data_int
float = $user_data_float
sci = $user_data_sci
bool = $user_data_bool
undef = $atata
'''

        with open(self.template_path, 'w') as f:
            f.write(tmpl)

    def tearDown(self):
        shutil.rmtree(self.local_dir)
        shutil.rmtree(self.remote_dir)
        os.remove(self.template_path)


    def call_remove_host(self, hostname):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.remove_host(hostname)

    def call_list_hosts(self):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.list_hosts()

    def call_submit_job(self, job):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.submit_job(job)

    def call_stop_job(self, id):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.stop_job(id)

    def call_cleanup_job(self, l, r):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.cleanup_job(l, r)

    def call_describe_job(self, id):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.describe_job(id)


    ############################################################
    # remove_host
    ############################################################

    def test__remove_host__called__command_is_executed(self):
        self.call_remove_host('host1')
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost remove host1', ))

    ############################################################
    # list_hosts 
    ############################################################

    def test__get_hosts__called__command_is_executed(self):
        self.call_list_hosts()
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost list_hosts', ))

    def test__get_hosts__rc_is_not_0__raises_and_logs_error(self):
        self.sp.returncode = 1

        with self.assertRaises(Exception):
            self.call_list_hosts()

        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__get_hosts__empty_output__returns_empty_list(self):
        self.out.decode.return_value = ''
        self.assertEqual(self.call_list_hosts(), [])

    def test__get_hosts__valid_output__returns_host_list(self):
        self.out.decode.return_value = '''
host0 Busy <127.0.0.0:9618?addrs=127.0.0.0-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
host1 Idle <127.0.0.1:9618?addrs=127.0.0.1-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
host2 Benchmarking <127.0.0.2:9618?addrs=127.0.0.2-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
        '''

        hosts = [
            {'name': 'host0', 'state': 'busy', 'ip': '127.0.0.0'},
            {'name': 'host1', 'state': 'idle', 'ip': '127.0.0.1'},
            {'name': 'host2', 'state': 'unavailable', 'ip': '127.0.0.2'},
        ]

        self.assertEqual(self.call_list_hosts(), hosts)

    def test__get_hosts__with_slots__returns_host_list(self):
        self.out.decode.return_value = '''
slot1@host0 Busy <127.0.0.0:9618?addrs=127.0.0.0-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
slot2@host0 Busy <127.0.0.0:9618?addrs=127.0.0.0-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
slot3@host0 Busy <127.0.0.0:9618?addrs=127.0.0.0-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
host1 Idle <127.0.0.1:9618?addrs=127.0.0.1-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
slot1@host2 Benchmarking <127.0.0.2:9618?addrs=127.0.0.2-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
slot2@host2 Benchmarking <127.0.0.2:9618?addrs=127.0.0.2-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
        '''

        hosts = [
            {'name': 'host0', 'state': 'busy', 'ip': '127.0.0.0'},
            {'name': 'host1', 'state': 'idle', 'ip': '127.0.0.1'},
            {'name': 'host2', 'state': 'unavailable', 'ip': '127.0.0.2'},
        ]

        self.assertEqual(self.call_list_hosts(), hosts)

    def test__get_hosts__single_host__returns_this_host(self):
        self.out.decode.return_value = '''
host0 Busy <127.0.0.0:9618?addrs=127.0.0.0-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
        '''

        hosts = [
            {'name': 'host0', 'state': 'busy', 'ip': '127.0.0.0'},
        ]

        self.assertEqual(self.call_list_hosts(), hosts)

    def test__get_hosts__number_of_fileds_is_not_3__logs_error_line_skipped(self):
        self.out.decode.return_value = '''
host0 Busy <127.0.0.0:9618?addrs=127.0.0.0-9618+[--1]-9618&noUDP&sock=72199_66bb_5> atata
host1 Busy <127.0.0.1:9618?addrs=127.0.0.1-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
host2 Busy
host3 

host4 Busy <127.0.0.4:9618?addrs=127.0.0.3-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
        '''

        hosts = [
            {'name': 'host1', 'state': 'busy', 'ip': '127.0.0.1'},
            {'name': 'host4', 'state': 'busy', 'ip': '127.0.0.4'},
        ]

        self.assertEqual(self.call_list_hosts(), hosts)
        self.assertNotEqual(self.logger.warning.call_args_list, [])

    def test__get_hosts__no_ip_in_field_3__logs_error_line_skipped(self):
        self.out.decode.return_value = '''
host1 Busy <127.0.999.0:9618?addrs=127.0.999.0-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
host4 Busy <127.0.0.4:9618?addrs=127.0.0.4-9618+[--1]-9618&noUDP&sock=72199_66bb_5>
        '''

        hosts = [
            {'name': 'host4', 'state': 'busy', 'ip': '127.0.0.4'},
        ]

        self.assertEqual(self.call_list_hosts(), hosts)
        self.assertNotEqual(self.logger.warning.call_args_list, [])

    ####################
    ## create local job
    ####################

    def test__create_job__valid_request__local_file_created(self):
        self.scheduler.command_template = '%(command)s'

        job = HTCondor.Job(42)
        job.set_resources(10, 4, 4096, None)
        job.set_hostnames(['n1', 'n2', 'n3'])
        job.set_user_data({
            'user_data_str': 'str1',
            'user_data_int': 42,
            'user_data_float': 4.2,
            'user_data_sci': 1e-99,
            'user_data_bool': True,
        })

        localdir, remotedir = self.scheduler.create_job(job)

        self.assertEqual(localdir, self.local_dir + '/42')
        self.assertEqual(localdir, self.remote_dir + '/42')

        expected_job = '''42
./remote/42/
output
error
log
10
4
4096
((Machine == "n1") || (Machine == "n2") || (Machine == "n3"))
str = str1
int = 42
float = 4.2
sci = 1e-99
bool = True
undef = $atata
'''

        with open(self.local_dir + '/42/job') as f:
            jobfile = f.read()

        self.assertEqual(jobfile, expected_job)

    def test__create_job__valid_request__local_file_created(self):
        tmpl = '''$request_id
$root
'''

        job = HTCondor.Job(42, tmpl)
        job.set_resources(10, 4, 4096, None)
        job.set_hostnames(['n1', 'n2', 'n3'])
        job.set_user_data({
            'user_data_str': 'str1',
            'user_data_int': 42,
            'user_data_float': 4.2,
            'user_data_sci': 1e-99,
            'user_data_bool': True,
        })

        localdir, remotedir = self.scheduler.create_job(job)

        self.assertEqual(localdir, self.local_dir + '42')
        self.assertEqual(remotedir, self.remote_dir + '42')

        expected_job = '''42
./remote/42
'''

        with open(self.local_dir + '/42/job') as f:
            job = f.read()

        self.assertEqual(job, expected_job)

    def test__transfer_job__valid_request__dir_is_copied(self):
        self.scheduler.command_template = '%(command)s'

        expected_job = 'atata'

        os.makedirs(self.local_dir + '/42/')
        with open(self.local_dir + '/42/job', 'w') as f:
            f.write(expected_job)

        remotedir = self.scheduler.transfer_job(42)
        self.assertEqual(remotedir, self.remote_dir + '42')

        with open(self.remote_dir + '/42/job') as f:
            job = f.read()

        self.assertEqual(job, expected_job)

    def test__submit_job__called__command_executed(self):
        self.call_submit_job('42/')
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost submit 42/job', ))

    def test__submit_job__valid_output__id_added(self):
        self.out.decode.return_value = '''
42.9 - 42.9
        '''
        self.assertEqual(self.call_submit_job('/'), 42)

    def test__submit_job__valid_output_no_dots__id_returned(self):
        self.out.decode.return_value = '''
42 - 42
        '''
        self.assertEqual(self.call_submit_job('/'), 42)

    def test__submit_job__valid_output_singel_id__id_returned(self):
        self.out.decode.return_value = '''
42.9
        '''
        self.assertEqual(self.call_submit_job('/'), 42)

    def test__submit_job__valid_output_singel_id_no_dots_id_returned(self):
        self.out.decode.return_value = '''
42 
        '''
        self.assertEqual(self.call_submit_job('/'), 42)

    def test__submit_job__range_returned__throws(self):
        self.out.decode.return_value = '''
10.0 - 42.0
        '''

        with self.assertRaises(Exception):
            self.call_submit_job('/')

    def test__submit_job__no_output__throws(self):
        self.out.decode.return_value = '''
        '''

        with self.assertRaises(Exception):
            self.call_submit_job('/')

    def test__submit_job__not_valid_output__throws(self):
        self.out.decode.return_value = '''
something
        '''

        with self.assertRaises(Exception):
            self.call_submit_job('/')

    def test__submit_job__no__throws(self):
        self.out.decode.return_value = '''

        '''

        with self.assertRaises(Exception):
            self.call_submit_job('/')

    def test__submit_job__fails__throws(self):
        self.sp.returncode = 1
        with self.assertRaises(Exception):
            self.call_submit_job('/')

#     def test__submit_job__file_not_found__throws(self):
#         # TODO: implement
#         pass

    def test__stop_job__called__command_executed_status_changed_to_done(self):
        self.call_stop_job(10)
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost remove 10', ))

    # def test__stop_job__fails__throws(self):
    #     self.sp.returncode = 1
    #     with self.assertRaises(Exception):
    #         self.call_stop_job(10)

    def test__cleanup_job__have_local_dir__localdir_removed(self):
        self.call_cleanup_job('l', None)
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('rm l', ))
#
    def test__cleanup_job__have_remote_dir__remotedir_removed(self):
        self.call_cleanup_job(None, 'r')
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost rm r', ))

    def test__describe_job__called__command_executed(self):
        self.call_describe_job(10)
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost describe 10', ))

    def test__describe_job__fails__throws(self):
        self.sp.returncode = 1
        with self.assertRaises(Exception):
            self.call_describe_job(10)

    def test__describe_job__valid_output__dict_returned(self):
        self.out.decode.return_value = '''
Cmd = "/home/mpi/dummy.sh"
Err = "errors.txt"
In = "/dev/null"
Out = "output.txt"
Iwd = "/tmp/"
Arguments = "1 2"
JobStatus = 2
ProcId = 0
ClusterId = 10
        '''

        job = {
            'id': 10,
            'status': 'running',
            'input': '/dev/null',
            'output': 'output.txt',
            'error': 'errors.txt',
            'workdir': '/tmp/',
            'command': '/home/mpi/dummy.sh',
            'arguments': '1 2'
        }

        self.assertEqual(self.call_describe_job(10), job)

    def test__describe_job__missing_fields__skipped(self):
        self.out.decode.return_value = '''
Cmd = "/home/mpi/dummy.sh"
Err = "errors.txt"
In = "/dev/null"
ProcId = 0
ClusterId = 10
AAA = 10
        '''

        job = {
            'id': 10,
            'input': '/dev/null',
            'error': 'errors.txt',
            'command': '/home/mpi/dummy.sh',
            'status': 'done'
        }

        self.assertEqual(self.call_describe_job(10), job)


    def test__describe_job__empty_output__status_done(self):
        self.out.decode.return_value = '''
'''

        job = {
            'id': 10,
            'status': 'done'
        }

        self.assertEqual(self.call_describe_job(10), job)

if __name__ == '__main__':
    unittest.main()
