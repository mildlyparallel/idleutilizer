import os
import sys

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from libs.clouds.opennebula import OpenNebula
from libs.clouds.opennebula import init_handler

class ONECloudTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.config = Mock()
        self.config.get_values.return_value = { }

    def tearDown(self):
        del self.logger
        del self.config

    def test__init_handler__called__create_api_object(self):
        mock = Mock()
        with patch('libs.clouds.opennebula.OpenNebula', mock):
            init_handler(self.logger, self.config)
        mock.assert_called_with(self.logger, self.config)

    def test__ctor__called__params_are_requestd(self):
        one = OpenNebula(self.config, self.logger)

        args, kwargs = self.config.get_values.call_args 
        section, requested = args

        self.assertEqual(section, one.section)

        for p in requested:
            self.assertIn(p, one.params)

class ONECloudSpawnTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.logger.error = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'endpoint': 'test-endpoint',
            'one_auth': 'test-auth',
            'template_id': '1142',
            'spawn_rpc_method': 'spawn',
            'spawn_rpc_args': "['arg-0', 42, {'a': [1, 2]}, '%(override)s']",
            'spawn_rpc_filter': '',
        }

        self.rpc = Mock()

    def call_spawn(self, params = None):
        proxy = Mock(return_value = self.rpc)
        with patch('xmlrpc.client.ServerProxy', proxy):
            self.one = OpenNebula(self.config, self.logger)
            return self.one.spawn_host(params)

    def tearDown(self):
        del self.rpc
        del self.one
        del self.logger
        del self.config

    def test__spawn_host__called_without_params__args_passed_ro_rpc(self):
        self.rpc.spawn.return_value = (True, 42, 0)
        self.call_spawn()
        args, kwargs = self.rpc.spawn.call_args 
        self.assertEqual(args, ('test-auth', 'arg-0', 42, {'a': [1, 2]}, ''))

    def test__spawn_host__called_with_memory_param__param_parsed(self):
        self.rpc.spawn.return_value = (True, 42, 0)
        self.call_spawn({'memory': 4096})
        args, kwargs = self.rpc.spawn.call_args 
        self.assertEqual(args, ('test-auth', 'arg-0', 42, {'a': [1, 2]},
            'MEMORY=4096\n'))

    def test__spawn_host__called_with_valid_params__params_parsed(self):
        self.rpc.spawn.return_value = (True, 42, 0)
        self.call_spawn({'memory': 4096, 'cpu': 4})
        args, kwargs = self.rpc.spawn.call_args 

        self.assertEqual(args[0], 'test-auth')
        self.assertEqual(args[1], 'arg-0')
        self.assertEqual(args[2], 42)
        self.assertEqual(args[3], {'a': [1, 2]})

        for i in args[4].split('\n'):
            self.assertIn(i, ['MEMORY=4096','CPU=4','VCPU=4', ''])

    def test__spawn_host__rpc_fails__throws_logs_error(self):
        self.rpc.spawn.return_value = (False, 'str', 0)
        with self.assertRaises(Exception):
            self.call_spawn()
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__spawn_host__rpc_fails_4_values_returned__throws_logs_error(self):
        self.rpc.spawn.return_value = (False, 'str', 0, -1)
        with self.assertRaises(Exception):
            self.call_spawn()
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__spawn_host__rpc_throws__throws_logs_error(self):
        self.rpc.spawn.side_effect = Exception('test-exception')
        with self.assertRaises(Exception):
            self.call_spawn()
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__spawn_host__rpc_returns_0__0_is_returned(self):
        self.rpc.spawn.return_value = (True, 0, 0)
        self.assertEqual(self.call_spawn(), 0)

    def test__spawn_host__rpc_returns_id__id_is_returned(self):
        self.rpc.spawn.return_value = (True, 42, 0)
        self.assertEqual(self.call_spawn(), 42)

    def test__spawn_host__rpc_returns_not_valid__throws(self):
        self.rpc.spawn.return_value = (True, 'xmla', 0)
        with self.assertRaises(Exception):
            self.call_spawn()

    def test__spawn_host__rpc_returns_not_string__throws(self):
        self.rpc.spawn.return_value = (True, '<xml><i>aa</i></xml>', 0)
        with self.assertRaises(Exception):
            self.call_spawn()

    def test__spawn_host__filter_fails__throws(self):
        self.rpc.spawn.return_value = (True, '<xml><i>42</i></xml>', 0)
        filter = 'to_number(xml.x)'
        self.config.get_values.return_value['spawn_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_spawn()

    def test__spawn_host__empty_after_filter__throws(self):
        self.rpc.spawn.return_value = (True, '<xml><i></i></xml>', 0)
        filter = 'to_number(xml.i)'
        self.config.get_values.return_value['spawn_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_spawn()

    def test__spawn_host__filtered_wrong_type__throws(self):
        self.rpc.spawn.return_value = (True, '<xml><i>str</i></xml>', 0)
        filter = 'xml.i'
        self.config.get_values.return_value['spawn_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_spawn()

    def test__spawn_host__valid_filter__returns_id(self):
        self.rpc.spawn.return_value = (True, '<xml><i>42</i></xml>', 0)
        filter = 'to_number(xml.i)'
        self.config.get_values.return_value['spawn_rpc_filter'] = filter

        self.assertEqual(self.call_spawn(), 42)

class ONECloudGetHostsTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.logger.error = Mock()
        self.logger.warning = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'endpoint': 'test-endpoint',
            'one_auth': 'test-auth',
            'get_hosts_rpc_method': 'get_hosts',
            'get_hosts_rpc_args': "['arg-0', 42, {'a': [1, 2]}]",
            'get_hosts_rpc_filter': '',
        }

        self.rpc = Mock()

    def call_get_hosts(self):
        proxy = Mock(return_value = self.rpc)
        with patch('xmlrpc.client.ServerProxy', proxy):
            self.one = OpenNebula(self.config, self.logger)
            return self.one.get_hosts()

    def tearDown(self):
        del self.rpc
        del self.one
        del self.logger
        del self.config

    def test__get_hosts__called__args_passed_ro_rpc(self):
        payload = '<xml><hosts></hosts></xml>'
        self.rpc.get_hosts.return_value = (True, payload, 0)
        filter = '([xml.hosts.host] || xml.hosts.host)[].{id: to_number(id), ip: ip}'
        self.config.get_values.return_value['get_hosts_rpc_filter'] = filter

        self.call_get_hosts()
        args, kwargs = self.rpc.get_hosts.call_args 
        self.assertEqual(args, ('test-auth', 'arg-0', 42, {'a': [1, 2]}))

    def test__get_hosts__rpc_fails__throws_logs(self):
        self.rpc.get_hosts.return_value = (False, 'str', 0)
        with self.assertRaises(Exception):
            self.call_get_hosts()
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__get_hosts__rpc_fails_with_4_values__throws_logs(self):
        self.rpc.get_hosts.return_value = (False, 'str', 0, -1)
        with self.assertRaises(Exception):
            self.call_get_hosts()
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__get_hosts__rpc_throws__throws_logs(self):
        self.rpc.get_hosts.side_effect = Exception('test-exception')
        with self.assertRaises(Exception):
            self.call_get_hosts()
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__get_hosts__rpc_returns_valid_hosts__list_returned(self):
        hosts = []
        payload = '<xml><hosts>'

        for i in range(1, 10):
            hosts.append({'id': i, 'ip': '127.0.0.' + str(i)})
            payload += '<host><id>' + str(i) + '</id><ip>127.0.0.' + str(i) + '</ip></host>'

        payload += '</hosts></xml>'

        filter = '([xml.hosts.host] || xml.hosts.host)[].{id: to_number(id), ip: ip}'
        self.config.get_values.return_value['get_hosts_rpc_filter'] = filter

        self.rpc.get_hosts.return_value = (True, payload, 0)
        self.assertEqual(self.call_get_hosts(), hosts)

    def test__get_hosts__rpc_returns_empty_hosts__empty_list_returned(self):
        payload = '<xml><hosts></hosts></xml>'
        self.rpc.get_hosts.return_value = (True, payload, 0)

        filter = '([xml.hosts.host] || xml.hosts.host)[].{id: to_number(id), ip: ip}'
        self.config.get_values.return_value['get_hosts_rpc_filter'] = filter

        self.assertEqual(self.call_get_hosts(), [])

    def test__get_hosts__one_host_in_list__returns_list_with_one_host(self):
        payload = '''
        <xml><hosts>
            <host><id>1</id><ip>127.0.0.1</ip></host>
        </hosts></xml>
        '''

        filter = '([xml.hosts.host] || xml.hosts.host)[].{id: to_number(id), ip: ip}'
        self.config.get_values.return_value['get_hosts_rpc_filter'] = filter

        self.rpc.get_hosts.return_value = (True, payload, 0)

        self.assertEqual(self.call_get_hosts(), [{'id': 1, 'ip': '127.0.0.1'}])

    def test__get_hosts__filter_fails__logs_warning_throws(self):
        payload = '<xml><hosts></hosts></xml>'
        self.rpc.get_hosts.return_value = (True, payload, 0)

        self.config.get_values.return_value['get_hosts_rpc_filter'] = 'h'

        with self.assertRaises(Exception):
            self.call_get_hosts()

    def test__get_hosts__rpc_returns_not_valid__throws(self):
        self.rpc.get_hosts.return_value = (True, 'xmla', 0)
        with self.assertRaises(Exception):
            self.call_get_hosts()

    def test__get_hosts__filtered_wrong_id_type__throws(self):
        payload = '''
        <xml><hosts>
            <host><id>1</id><ip>127.0.0.2</ip></host>
        </hosts></xml>
        '''

        filter = '([xml.hosts.host] || xml.hosts.host)[].{id: id, ip: ip}'
        self.config.get_values.return_value['get_hosts_rpc_filter'] = filter

        self.rpc.get_hosts.return_value = (True, payload, 0)

        with self.assertRaises(Exception):
            self.call_get_hosts()

class ONECloudRemoveHostTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.logger.error = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'endpoint': 'test-endpoint',
            'one_auth': 'test-auth',
            'remove_rpc_method': 'remove',
            'remove_rpc_args': "['delete', %(id)s]",
            'remove_rpc_filter': '',
        }

        self.rpc = Mock()

    def call_remove(self, id):
        proxy = Mock(return_value = self.rpc)
        with patch('xmlrpc.client.ServerProxy', proxy):
            self.one = OpenNebula(self.config, self.logger)
            return self.one.remove_host(id)

    def tearDown(self):
        del self.rpc
        del self.one
        del self.logger
        del self.config

    def test__remove_host__called__args_passed_ro_rpc_id_substituded(self):
        self.rpc.remove.return_value = (True, 42, 0)
        self.call_remove(10)
        args, kwargs = self.rpc.remove.call_args 
        self.assertEqual(args, ('test-auth', 'delete', 10))

    def test__remove_host__id_is_none__throws(self):
        with self.assertRaises(Exception):
            self.call_remove(None)

    def test__remove_host__id_has_wrong_type__throws(self):
        with self.assertRaises(Exception):
            self.call_remove('a')

    def test__remove_host__rpc_fails__throws_logs(self):
        self.rpc.remove.return_value = (False, 'str', 0)
        with self.assertRaises(Exception):
            self.call_remove(0)
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__remove_host__rpc_fails_with_4_files__throws_logs(self):
        self.rpc.remove.return_value = (False, 'str', 0, -1)
        with self.assertRaises(Exception):
            self.call_remove(0)
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__remove_host__rpc_throws__throws_logs(self):
        self.rpc.remove.side_effect = Exception('test-exception')
        with self.assertRaises(Exception):
            self.call_remove(0)
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__remove_host__rpc_throws__logs_error(self):
        self.rpc.remove.side_effect = Exception('test-exception')
        with self.assertRaises(Exception):
            self.call_remove(0)
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__remove_host__rpc_returns_0__0_is_returned(self):
        self.rpc.remove.return_value = (True, 0, 0)
        self.assertEqual(self.call_remove(0), 0)

    def test__remove_host__rpc_returns_id__id_is_returned(self):
        self.rpc.remove.return_value = (True, 42, 0)
        self.assertEqual(self.call_remove(0), 42)

    def test__remove_host__rpc_returns_not_valid__throws(self):
        self.rpc.remove.return_value = (True, 'xmla', 0)
        with self.assertRaises(Exception):
            self.call_remove(0)

    def test__remove_host__rpc_returns_not_string__throws(self):
        self.rpc.remove.return_value = (True, '<xml><i>aa</i></xml>', 0)
        with self.assertRaises(Exception):
            self.call_remove(0)

    def test__remove_host__filter_fails__throws(self):
        self.rpc.remove.return_value = (True, '<xml><i>42</i></xml>', 0)
        filter = 'to_number(xml.x)'
        self.config.get_values.return_value['remove_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_remove(0)

    def test__remove_host__empty_after_filter__throws(self):
        self.rpc.remove.return_value = (True, '<xml><i></i></xml>', 0)
        filter = 'to_number(xml.i)'
        self.config.get_values.return_value['remove_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_remove(0)

    def test__remove_host__filtered_wrong_type__throws(self):
        self.rpc.remove.return_value = (True, '<xml><i>str</i></xml>', 0)
        filter = 'xml.i'
        self.config.get_values.return_value['remove_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_remove(0)

    def test__remove_host__valid_filter__returns_id(self):
        self.rpc.remove.return_value = (True, '<xml><i>42</i></xml>', 0)
        filter = 'to_number(xml.i)'
        self.config.get_values.return_value['remove_rpc_filter'] = filter

        self.assertEqual(self.call_remove(0), 42)

class ONECloudChownHostTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.logger.error = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'endpoint': 'test-endpoint',
            'one_auth': 'test-auth',
            'chown_rpc_method': 'chown',
            'chown_rpc_args': "[%(id)s, %(uid)s, %(gid)s]",
            'chown_rpc_filter': '',
        }

        self.rpc = Mock()

    def call_chown(self, id, owner):
        proxy = Mock(return_value = self.rpc)
        with patch('xmlrpc.client.ServerProxy', proxy):
            self.one = OpenNebula(self.config, self.logger)
            return self.one.chown_host(id, owner)

    def tearDown(self):
        del self.rpc
        # del self.one
        del self.logger
        del self.config

    def test__chown_host__called__args_passed_ro_rpc_id_substituded(self):
        self.rpc.chown.return_value = (True, 42, 0)
        self.call_chown(1, {'uid': 2, 'gid': 3})
        args, kwargs = self.rpc.chown.call_args 
        self.assertEqual(args, ('test-auth', 1, 2, 3))

    def test__chown_host__id_is_none__throws(self):
        with self.assertRaises(Exception):
            self.call_chown(None)

    def test__chown_host__uid_is_none__uid_set_to_default(self):
        self.rpc.chown.return_value = (True, 42, 0)
        self.call_chown(1, {'uid': None, 'gid': 3})
        args, kwargs = self.rpc.chown.call_args 
        self.assertEqual(args, ('test-auth', 1, -1, 3))

    def test__chown_host__gid_is_none__gid_set_to_default(self):
        self.rpc.chown.return_value = (True, 42, 0)
        self.call_chown(1, {'uid': 2, 'gid': None})
        args, kwargs = self.rpc.chown.call_args 
        self.assertEqual(args, ('test-auth', 1, 2, -1))

    def test__chown_host__uid_andgid_is_none__set_to_default(self):
        self.rpc.chown.return_value = (True, 42, 0)
        self.call_chown(1, {'uid': None, 'gid': None})
        args, kwargs = self.rpc.chown.call_args 
        self.assertEqual(args, ('test-auth', 1, -1, -1))

    def test__chown_host__id_has_wrong_type__throws(self):
        with self.assertRaises(Exception):
            self.call_chown('a', {'uid': 2, 'gid': 3})

    def test__chown_host__uid_has_wrong_type__throws(self):
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 'a', 'gid': 3})

    def test__chown_host__gid_has_wrong_type__throws(self):
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 'a'})

    def test__chown_host__rpc_fails__throws_logs(self):
        self.rpc.chown.return_value = (False, 'str', 0)
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__chown_host__rpc_fails_4_values__throws_logs(self):
        self.rpc.chown.return_value = (False, 'str', 0, -1)
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__chown_host__rpc_throws__throws_logs(self):
        self.rpc.chown.side_effect = Exception('test-exception')
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__chown_host__rpc_throws__logs_error(self):
        self.rpc.chown.side_effect = Exception('test-exception')
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})
        self.assertNotEqual(self.logger.error.call_args_list, [])

    def test__chown_host__rpc_returns_0__0_is_returned(self):
        self.rpc.chown.return_value = (True, 0, 0)
        self.assertEqual(self.call_chown(1, {'uid': 2, 'gid': 3}), 0)

    def test__chown_host__rpc_returns_id__id_is_returned(self):
        self.rpc.chown.return_value = (True, 42, 0)
        self.assertEqual(self.call_chown(1, {'uid': 2, 'gid': 3}), 42)

    def test__chown_host__rpc_returns_not_valid__throws(self):
        self.rpc.chown.return_value = (True, 'xmla', 0)
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})

    def test__chown_host__rpc_returns_not_string__throws(self):
        self.rpc.chown.return_value = (True, '<xml><i>aa</i></xml>', 0)
        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})

    def test__chown_host__filter_fails__throws(self):
        self.rpc.chown.return_value = (True, '<xml><i>42</i></xml>', 0)
        filter = 'to_number(xml.x)'
        self.config.get_values.return_value['chown_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})

    def test__chown_host__empty_after_filter__throws(self):
        self.rpc.chown.return_value = (True, '<xml><i></i></xml>', 0)
        filter = 'to_number(xml.i)'
        self.config.get_values.return_value['chown_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})

    def test__chown_host__filtered_wrong_type__throws(self):
        self.rpc.chown.return_value = (True, '<xml><i>str</i></xml>', 0)
        filter = 'xml.i'
        self.config.get_values.return_value['chown_rpc_filter'] = filter

        with self.assertRaises(Exception):
            self.call_chown(1, {'uid': 2, 'gid': 3})

    def test__chown_host__valid_filter__returns_id(self):
        self.rpc.chown.return_value = (True, '<xml><i>42</i></xml>', 0)
        filter = 'to_number(xml.i)'
        self.config.get_values.return_value['chown_rpc_filter'] = filter

        self.assertEqual(self.call_chown(1, {'uid': 2, 'gid': 3}), 42)

if __name__ == '__main__':
    unittest.main()
