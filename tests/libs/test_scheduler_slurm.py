import os
import stat
import sys
import shutil

path = os.path.abspath(__file__)
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(path)))
os.sys.path.insert(0, parentdir)

from pprint import pprint as pp

import unittest
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import call

from libs.schedulers.slurm import Slurm
from libs.schedulers.slurm import init_handler

class SlurmTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock()
        self.config = Mock()
        self.config.get_values.return_value = {
            'default_template': 'a',
        }

    def tearDown(self):
        del self.logger
        del self.config

    def test__init_handler__called__create_api_object(self):
        mock = Mock()
        with patch('libs.schedulers.slurm.Slurm', mock):
            init_handler(self.config, self.logger)
        mock.assert_called_with(self.config, self.logger)

    def test__ctor__called__params_are_requestd(self):
        scheduler = Slurm(self.config, self.logger)

        args, kwargs = self.config.get_values.call_args 
        section, requested = args

        self.assertEqual(section, scheduler.section)

        for p in requested:
            self.assertIn(p, scheduler.params)

class SlurmMethodTest(unittest.TestCase):
    local_dir = './local/'
    remote_dir = './remote/'
    template_path = 'job.tmpl'

    def setUp(self):
        self.logger = Mock()
        self.logger.error = Mock()
        self.logger.warning = Mock()

        self.config = Mock()
        self.config.get_values.return_value = {
            'host': 'localhost',
            'command_template': 'template %(host)s %(command)s',
            'command_remove_host': 'remove %(hostname)s',
            'command_list_hosts': 'list_hosts',
            'command_submit_job': 'submit %(remote_path)s',
            'command_stop_job': 'remove %(id)s',
            'command_describe_job': 'describe %(id)s',
            'command_copy': 'cp -r %(local_path)s %(remote_path)s',
            'local_root': self.local_dir,
            'remote_root': self.remote_dir,
            'default_template': self.template_path,
            'command_rm': 'rm %(path)s',
        }

        self.out = Mock()
        self.out.decode.return_value = '0'

        self.sp = Mock()
        self.sp.communicate.return_value = (self.out, 'err')
        self.sp.pid = 100
        self.sp.returncode = 0

        self.popen = Mock(return_value=self.sp)

        self.scheduler = Slurm(self.config, self.logger)

        if not os.path.exists(self.local_dir):
            os.makedirs(self.local_dir)

        if not os.path.exists(self.remote_dir):
            os.makedirs(self.remote_dir)

        tmpl = '''$request_id
$root
$output
$error
$log
$nodes
$cpu
$memory
$time
str = $user_data_str
int = $user_data_int
float = $user_data_float
sci = $user_data_sci
bool = $user_data_bool
undef = $atata
'''

        with open(self.template_path, 'w') as f:
            f.write(tmpl)

    def tearDown(self):
        shutil.rmtree(self.local_dir)
        shutil.rmtree(self.remote_dir)
        os.remove(self.template_path)

    def call_submit_job(self, job):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.submit_job(job)

    def call_stop_job(self, id):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.stop_job(id)

    def call_cleanup_job(self, l, r):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.cleanup_job(l, r)

    def call_describe_job(self, id):
        with patch('subprocess.Popen', self.popen):
            return self.scheduler.describe_job(id)

#     ####################
#     ## create local job
#     ####################

    def test__create_job__valid_request__local_file_created(self):
        self.scheduler.command_template = '%(command)s'

        job = Slurm.Job(42)
        job.set_resources(10, 4, 4096, 360)
        job.set_user_data({
            'user_data_str': 'str1',
            'user_data_int': 42,
            'user_data_float': 4.2,
            'user_data_sci': 1e-99,
            'user_data_bool': True,
        })

        localdir, remotedir = self.scheduler.create_job(job)

        self.assertEqual(localdir, self.local_dir + '/42')
        self.assertEqual(localdir, self.remote_dir + '/42')

        expected_job = '''42
./remote/42/
output
error
log
10
4
4096
360
str = str1
int = 42
float = 4.2
sci = 1e-99
bool = True
undef = $atata
'''

        localjob = self.local_dir + '/42/job'

        with open(localjob) as f:
            jobfile = f.read()

        self.assertEqual(jobfile, expected_job)

    def test__create_job__valid_request__local_file_created(self):
        tmpl = '''$request_id
$root
'''

        job = Slurm.Job(42, tmpl)
        job.set_resources(10, 4, 4096, 360)
        job.set_user_data({
            'user_data_str': 'str1',
            'user_data_int': 42,
            'user_data_float': 4.2,
            'user_data_sci': 1e-99,
            'user_data_bool': True,
        })

        localdir, remotedir = self.scheduler.create_job(job)

        self.assertEqual(localdir, self.local_dir + '42')
        self.assertEqual(remotedir, self.remote_dir + '42')

        expected_job = '''42
./remote/42
'''
        localjob = self.local_dir + '/42/job'

        with open(localjob) as f:
            job = f.read()

        self.assertEqual(job, expected_job)

        self.assertTrue(os.access(localjob, os.X_OK))

    def test__transfer_job__valid_request__dir_is_copied(self):
        self.scheduler.command_template = '%(command)s'

        expected_job = 'atata'

        os.makedirs(self.local_dir + '/42/')
        with open(self.local_dir + '/42/job', 'w') as f:
            f.write(expected_job)

        tmpl = '''$request_id
$root
'''
        remotedir = self.scheduler.transfer_job(42)
        self.assertEqual(remotedir, self.remote_dir + '42')

        remotejob = self.remote_dir + '/42/job'

        with open(remotejob) as f:
            job = f.read()

        self.assertEqual(job, expected_job)

    def test__submit_job__called__command_executed(self):
        self.call_submit_job('42/')
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost submit 42/job', ))

    def test__submit_job__valid_output__id_added(self):
        self.out.decode.return_value = '''
Submitted batch job 24405
        '''
        self.assertEqual(self.call_submit_job('/'), 24405)

    def test__submit_job__no_output__throws(self):
        self.out.decode.return_value = '''
        '''

        with self.assertRaises(Exception):
            self.call_submit_job('/')

    def test__submit_job__not_valid_output__throws(self):
        self.out.decode.return_value = '''
something
        '''

        with self.assertRaises(Exception):
            self.call_submit_job('/')

    def test__submit_job__no_output__throws(self):
        self.out.decode.return_value = '''

        '''

        with self.assertRaises(Exception):
            self.call_submit_job('/')

    def test__submit_job__fails__throws(self):
        self.sp.returncode = 1
        with self.assertRaises(Exception):
            self.call_submit_job('/')

# #     def test__submit_job__file_not_found__throws(self):
# #         # TODO: implement
# #         pass
#
    def test__stop_job__called__command_executed_status_changed_to_done(self):
        self.call_stop_job(10)
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost remove 10', ))

    # def test__stop_job__fails__throws(self):
    #     self.sp.returncode = 1
    #     with self.assertRaises(Exception):
    #         self.call_stop_job(10)

    def test__cleanup_job__have_local_dir__localdir_removed(self):
        self.call_cleanup_job('l', None)
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('rm l', ))

    def test__cleanup_job__have_remote_dir__remotedir_removed(self):
        self.call_cleanup_job(None, 'r')
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost rm r', ))

    def test__describe_job__called__command_executed(self):
        self.call_describe_job(10)
        args, kwargs = self.popen.call_args
        self.assertEqual(args, ('template localhost describe 10', ))

    def test__describe_job__fails__throws(self):
        self.sp.returncode = 1
        with self.assertRaises(Exception):
            self.call_describe_job(10)

    def test__describe_job__valid_output__dict_returned(self):
        self.out.decode.return_value = '''
JobId=10 JobName=job.sh UserId=kuchumov(10472) GroupId=hybrilit(10001) MCS_label=N/A Priority=4294889077 Nice=0 JobState=PENDING Reason=Priority ExitCode=0:0 RunTime=00:00:00 TimeLimit=01:00:00 TimeMin=N/A SubmitTime=2018-05-29T15:25:40 EligibleTime=2018-05-29T15:25:40 ReqB:S:C:T=0:0:*:* Power=
        '''

        job = {
            'id': 10,
            'status': 'idle',
        }

        self.assertEqual(self.call_describe_job(10), job)

    def test__describe_job__missing_fields__skipped(self):
        self.out.decode.return_value = '''
JobId=10
        '''

        job = {
            'id': 10,
            'status': 'done'
        }

        self.assertEqual(self.call_describe_job(10), job)

    def test__describe_job__empty_output__status_done(self):
        self.out.decode.return_value = '''
'''

        job = {
            'id': 10,
            'status': 'done'
        }

        self.assertEqual(self.call_describe_job(10), job)

if __name__ == '__main__':
    unittest.main()
