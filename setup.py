#!/usr/bin/env python3

from setuptools import setup, find_packages
from os import path

import src

VERSION = src.__version__

here = path.abspath(path.dirname(__file__))

requirements = [
    "xmltodict",
    "daemonize",
    "jmespath",
    "tinydb"
]

from setuptools.command.install_scripts import install_scripts

setup(name='idleutilizer',
      version=VERSION,
      description='',
      author='Ruslan Kuchumov',
      author_email='kuchumovri@gmail.com',
      url='https://git.jinr.ru/cloud-team/SmartScheduler-IdleUtilizer',
      python_requires='~=3.4',


      classifiers=[
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.3',
          'Programming Language :: Python :: 3.4',
          'Programming Language :: Python :: 3.5',
      ],

      keywords='cloud api scheduler batch-scheduler',

      scripts=['idle-utilizer'],

      include_package_data=True,
      install_requires=requirements,
      extras_require={
          'dev': [
              'pytest',
              'pytest-pep8',
              'pytest-cov',
              'bumpversion',
              'wheel',
              'twine',
              'nose'
          ]
      },
      packages=find_packages(exclude=['tests']),
)
