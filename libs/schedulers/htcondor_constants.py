HOST_ACTIVITY_MAP = {
    'Idle': 'idle',
    'Busy': 'busy',
    '*': 'unavailable'
}

JOB_ACTIVITY_MAP = {
    '0': 'unexpanded',
    '1': 'idle',
    '2': 'running',
    '3': 'removed',
    '4': 'comleted',
    '5': 'held',
    '6': 'error'
}

JOB_DESCRIPTION_MAP = {
    'Cmd': 'command',
    'Err': 'error',
    'In': 'input',
    'Out': 'output',
    'Iwd': 'workdir',
    'Arguments': 'arguments',
    'JobStatus': 'status',
    'ClusterId': 'id'
}
