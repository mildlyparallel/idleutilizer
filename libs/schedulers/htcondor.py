import re
import os
import subprocess
from string import Template

from pprint import pprint as pp

from libs.schedulers.htcondor_constants import HOST_ACTIVITY_MAP
from libs.schedulers.htcondor_constants import JOB_ACTIVITY_MAP
from libs.schedulers.htcondor_constants import JOB_DESCRIPTION_MAP

def init_handler(config, logger):
    return HTCondor(config, logger)

def export_config():
    return (HTCondor.section, HTCondor.params)


class HTCondor():
    section = 'scheduler.htcondor'

    params = [
        {
            'name': 'host',
            'default': 'user@htcondor.submit.node',
            'type': str
        },
        {
            'name': 'command_template',
            'default': 'ssh %(host)s %(command)s',
            'type': str
        },
        {
            'name': 'command_list_hosts',
            'default': 'condor_status -autoformat Name Activity StartdIpAddr',
            'type': str
        },
        {
            'name': 'command_remove_host',
            'default': '/usr/sbin/condor_off %(hostname)s',
            'type': str
        },
        {
            'name': 'command_submit_job',
            'default': 'condor_submit -terse %(remote_path)s',
            'type': str
        },
        {
            'name': 'command_stop_job',
            'default': 'condor_rm %(id)s',
            'type': str
        },
        {
            'name': 'command_describe_job',
            'default': 'condor_q -long %(id)s',
            'type': str
        },
        {
            'name': 'default_template',
            'default': '/etc/idleutilizer/htcondor.tmpl',
            'type': str
        },
        {
            'name': 'command_copy',
            'default': 'scp -r %(local_path)s %(host)s:%(remote_path)s',
            'type': str
        },
        {
            'name': 'command_rm',
            'default': 'rm -rf %(path)s',
            'type': str
        },
        {
            'name': 'local_root',
            'default': '/tmp/',
            'type': str
        }, 
        {
            'name': 'remote_root',
            'default': '/tmp/',
            'type': str
        } 
    ]

    def __init__(self, config, logger):
        values = config.get_values(HTCondor.section, HTCondor.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._logger = logger

        # TODO: find proper way of passing values to the nested class
        HTCondor.logger = logger
        HTCondor.default_template = self.default_template

    class Job():
        def __init__(self, request_id, template = None):
            self.request_id = request_id

            if not template:
                self._template = self._read_template()
            else:
                self._template = Template(template)

            self._ctx = {
                'request_id': str(request_id)
            }

        def _read_template(self):
            try:
                with open(HTCondor.default_template) as f:
                    return Template(f.read())
            except IOError as e:
                HTCondor.logger.error('Can not read default template file: %s' % str(e))
                raise e

        def set_paths(self, remotedir):
            self._ctx.update({
                'root':    remotedir,
                'output':  os.path.join(remotedir, 'output'),
                'error':   os.path.join(remotedir, 'error'),
                'log':     os.path.join(remotedir, 'log'),
            })

        def set_resources(self, nodes, cpu, memory, time):
            self._ctx.update({
                'nodes':   str(nodes),
                'cpu':     str(cpu),
                'memory':  str(memory)
            })

        def set_hostnames(self, hostnames):
            if not hostnames:
                return

            r = [ '(Machine == "%s")' % h for h in hostnames ]

            requirements = ' || '.join(r)
            if len(r) > 1:
                requirements = '(%s)' % requirements

            self._ctx.update({
                'requirements':  requirements
            })

        def set_user_data(self, user_data):
            if not user_data:
                return

            for k, v in user_data.items():
                self._ctx[k] = v

        def get(self):
            return self._template.safe_substitute(self._ctx)

    def list_hosts(self):
        output = self._ssh_exec(self.command_list_hosts)

        hosts = []

        for line in output.split('\n'):
            if line == '':
                continue

            splitted = line.split(' ')
            if len(splitted) != 3:
                self._logger.warning('Failed to parse host line in the output of command_list_hosts: {}'
                    .format(line))
                continue

            ip = self._grep_ip(splitted[2])
            if not ip:
                self._logger.warning('Failed to parse IP address in the output of command_list_hosts: {}'
                    .format(line))
                continue

            name = splitted[0].split('@', 1)[-1]

            if any(h['name'] == name for h in hosts):
                continue

            host = {
                'name':   name,
                'state':  self._map(splitted[1], HOST_ACTIVITY_MAP),
                'ip':     ip
            }

            hosts.append(host)

        return hosts

    def remove_host(self, hostname):
        self._ssh_exec(self.command_remove_host % {'hostname': hostname})

    def create_job(self, job):
        localdir = os.path.join(self.local_root, str(job.request_id))
        remotedir = os.path.join(self.remote_root, str(job.request_id))

        job.set_paths(remotedir)

        if not os.path.exists(localdir):
            os.makedirs(localdir)

        localjob = os.path.join(localdir, 'job')
        with open(localjob, 'w') as f:
            f.write(job.get())

        return (localdir, remotedir)

    # TODO: pass something else, not request_id
    def transfer_job(self, request_id):
        localdir = os.path.join(self.local_root, str(request_id))
        self._copy_to_remote(localdir, self.remote_root)
        return os.path.join(self.remote_root, str(request_id))

    def submit_job(self, remotedir):
        remotejob = os.path.join(remotedir, 'job')
        cmd = self.command_submit_job % {'remote_path': remotejob}

        output = self._ssh_exec(cmd)
        first, last = self._parse_range(output)

        if first != last:
            raise Exception('Jobs id range is not supported')

        return first

    def describe_job(self, job_id):
        cmd = self.command_describe_job % {'id': job_id}
        output = self._ssh_exec(cmd)

        if output.strip() == '':
            return {'status': 'done', 'id': job_id}

        d = self._job_info_to_dict(output)
        return self._map_job_fields(d)

    def stop_job(self, job_id):
        cmd = self.command_stop_job % {'id': job_id}

        try:
            self._ssh_exec(cmd)
            # TODO: handle exception properly
        except Exception as e:
            pass

    def cleanup_job(self, localdir, remotedir):
        if localdir:
            cmd = self.command_rm % {'path': localdir}
            self._exec(cmd)

        if remotedir:
            cmd = self.command_rm % {'path': remotedir}
            self._ssh_exec(cmd)

    def cleanup_local(self, localdir):
        if localdir:
            cmd = self.command_rm % {'path': localdir}
            self._exec(cmd)

    def cleanup_remote(self, remotedir):
        if remotedir:
            cmd = self.command_rm % {'path': remotedir}
            self._ssh_exec(cmd)

    def _parse_range(self, output):
        ids = output.split(' - ')
        if len(ids) == 1:
            first = ids[0].split('.')[0]
            last = first
        elif len(ids) == 2:
            first = ids[0].split('.')[0]
            last = ids[1].split('.')[0]
        else:
            raise Exception('Cant parse IDs range')

        try:
            first = int(first)
            last = int(last)
        except:
            raise Exception('Cant parse IDs range')
        
        return (first, last)

    def _job_info_to_dict(slef, output):
        lines = output.split('\n')

        d = {}

        for l in lines:
            kv = l.split(' = ')
            if len(kv) != 2:
                continue

            k = kv[0]
            v = kv[1].strip('"')

            d[k] = v

        return d

    def _map_job_fields(self, job):
        ret = {}

        for src, dest in JOB_DESCRIPTION_MAP.items():
            if src not in job:
                continue

            ret[dest] = job[src]

        if 'status' in ret:
            ret['status'] = self._map(ret['status'], JOB_ACTIVITY_MAP)
        else:
            ret['status'] = 'done'
            self._logger.warning('Job status is not specified')

        if 'id' in ret:
            ret['id'] = int(ret['id'])

        return ret


    def _grep_ip(self, addr):
        ip_re = re.compile("(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(1[0-9]{2}|2[0-4][0-9]|25[0-5]|[1-9][0-9]|[0-9])")
        found = ip_re.search(addr)

        if not found:
            return None

        return found.group(0)

    def _copy_to_remote(self, src, dest):
        cmd = self.command_copy % {
            'host': self.host,
            'local_path': src,
            'remote_path': dest
        }

        self._exec(cmd)

    def _ssh_exec(self, cmd):
        cmd = self.command_template % {
            'command': cmd,
            'host': self.host
        }
        return self._exec(cmd)

    def _exec(self, cmd):
        self._logger.debug('Executing: ' + cmd)
        sp = subprocess.Popen(cmd, shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        (out, err) = sp.communicate(input=None)

        self._logger.debug('cmd pid:%s' % sp.pid)
        self._logger.debug('cmd ret:%s' % sp.returncode)
        self._logger.debug('cmd out:%s' % out)
        self._logger.debug('cmd err:%s' % err)

        if sp.returncode == 0:
            return out.decode('utf-8')

        self._logger.error('Command failed: %s' % cmd)
        self._logger.error('cmd pid:%s' % sp.pid)
        self._logger.error('cmd ret:%s' % sp.returncode)
        self._logger.error('cmd out:%s' % out)
        self._logger.error('cmd err:%s' % err)

        raise Exception('cmd: %s failed: %s' % (cmd, err)) 

    def _map(self, what, constants):
        if what in constants:
            return constants[what]
        else:
            return constants['*']

