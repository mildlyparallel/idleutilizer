import re
import os
import subprocess
from string import Template

from pprint import pprint as pp

def init_handler(config, logger):
    return Slurm(config, logger)

def export_config():
    return (Slurm.section, Slurm.params)


class Slurm():
    JOB_DESCRIPTION_MAP = {
        'JobId': 'id',
        'JobState': 'status'
    }

    JOB_ACTIVITY_MAP = {
        'CONFIGURING':  'idle',
        'PENDING':      'idle',
        'RUNNING':      'running',
        'COMPLETED':    'done',
        'CANCELLED':    'done',
        'FAILED':       'done'
    }
    
    section = 'scheduler.slurm'

    params = [
        {
            'name': 'host',
            'default': 'user@slurm.submit.node',
            'type': str
        },
        {
            'name': 'command_template',
            'default': 'ssh %(host)s %(command)s',
            'type': str
        },
        {
            'name': 'command_submit_job',
            'default': 'sbatch %(remote_path)s',
            'type': str
        },
        {
            'name': 'command_stop_job',
            'default': 'scancel %(id)s',
            'type': str
        },
        {
            'name': 'command_describe_job',
            'default': 'scontrol show job %(id)s --quiet --oneline',
            'type': str
        },
        {
            'name': 'default_template',
            'default': '/etc/idleutilizer/slurm.tmpl',
            'type': str
        },
        {
            'name': 'command_copy',
            'default': 'scp -r %(local_path)s %(host)s:%(remote_path)s',
            'type': str
        },
        {
            'name': 'command_rm',
            'default': 'rm -rf %(path)s',
            'type': str
        },
        {
            'name': 'local_root',
            'default': '/tmp/',
            'type': str
        }, 
        {
            'name': 'remote_root',
            'default': '/tmp/',
            'type': str
        } 
    ]

    def __init__(self, config, logger):
        values = config.get_values(Slurm.section, Slurm.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._logger = logger

        # TODO: find proper way of passing values to the nested class
        Slurm.logger = logger
        Slurm.default_template = self.default_template

    class Job():
        def __init__(self, request_id, template = None):
            self.request_id = request_id

            if not template:
                self._template = self._read_template()
            else:
                self._template = Template(template)

            self._ctx = {
                'request_id': str(request_id)
            }

        def _read_template(self):
            try:
                with open(Slurm.default_template) as f:
                    return Template(f.read())
            except IOError as e:
                Slurm.logger.error('Can not read default template file: %s' % str(e))
                raise e

        def set_paths(self, remotedir):
            self._ctx.update({
                'root':    remotedir,
                'output':  os.path.join(remotedir, 'output'),
                'error':   os.path.join(remotedir, 'error'),
                'log':     os.path.join(remotedir, 'log'),
            })

        def set_resources(self, nodes, cpu, memory, time):
            self._ctx.update({
                'nodes':   str(nodes),
                'cpu':     str(cpu),
                'memory':  str(memory),
                'time':    str(time),
            })

        def set_hostnames(self, hostnames):
            pass

        def set_user_data(self, user_data):
            if not user_data:
                return

            for k, v in user_data.items():
                self._ctx[k] = v

        def get(self):
            return self._template.safe_substitute(self._ctx)

    def list_hosts(self):
        raise Exception('Slurm.list_hosts() is not implemented')

    def remove_host(self, hostname):
        raise Exception('Slurm.remove_host() is not implemented')

    def create_job(self, job):
        localdir = os.path.join(self.local_root, str(job.request_id))
        remotedir = os.path.join(self.remote_root, str(job.request_id))

        job.set_paths(remotedir)

        if not os.path.exists(localdir):
            os.makedirs(localdir)

        localjob = os.path.join(localdir, 'job')
        with open(localjob, 'w') as f:
            f.write(job.get())

        os.chmod(localjob, 0o744)

        return (localdir, remotedir)

    # TODO: pass something else, not request_id
    def transfer_job(self, request_id):
        localdir = os.path.join(self.local_root, str(request_id))
        self._copy_to_remote(localdir, self.remote_root)
        return os.path.join(self.remote_root, str(request_id))

    def submit_job(self, remotedir):
        remotejob = os.path.join(remotedir, 'job')
        cmd = self.command_submit_job % {'remote_path': remotejob}

        output = self._ssh_exec(cmd)
        job_id = int(''.join(filter(str.isdigit, output)))

        return job_id

    def describe_job(self, job_id):
        cmd = self.command_describe_job % {'id': job_id}
        output = self._ssh_exec(cmd)

        if output.strip() == '':
            return {'status': 'done', 'id': job_id}

        d = self._job_info_to_dict(output)
        return self._map_job_fields(d)

    def stop_job(self, job_id):
        cmd = self.command_stop_job % {'id': job_id}

        try:
            self._ssh_exec(cmd)
            # TODO: handle exception properly
        except Exception as e:
            pass

    def cleanup_job(self, localdir, remotedir):
        if localdir:
            cmd = self.command_rm % {'path': localdir}
            self._exec(cmd)

        if remotedir:
            cmd = self.command_rm % {'path': remotedir}
            self._ssh_exec(cmd)

    def cleanup_local(self, localdir):
        if localdir:
            cmd = self.command_rm % {'path': localdir}
            self._exec(cmd)

    def cleanup_remote(self, remotedir):
        if remotedir:
            cmd = self.command_rm % {'path': remotedir}
            self._ssh_exec(cmd)

    def _job_info_to_dict(slef, output):
        lines = output.split(' ')

        d = {}

        for l in lines:
            kv = l.split('=')
            if len(kv) != 2:
                continue

            k = kv[0].strip('\n')
            v = kv[1].strip('\n')

            d[k] = v

        return d

    def _map_job_fields(self, job):
        ret = {}

        for src, dest in Slurm.JOB_DESCRIPTION_MAP.items():
            if src not in job:
                continue

            ret[dest] = job[src]

        if 'status' in ret:
            ret['status'] = self._map(ret['status'], Slurm.JOB_ACTIVITY_MAP)
        else:
            ret['status'] = 'done'
            self._logger.warning('Job status is not specified')

        if 'id' in ret:
            ret['id'] = int(ret['id'])

        return ret

    def _copy_to_remote(self, src, dest):
        cmd = self.command_copy % {
            'host': self.host,
            'local_path': src,
            'remote_path': dest
        }

        self._exec(cmd)

    def _ssh_exec(self, cmd):
        cmd = self.command_template % {
            'command': cmd,
            'host': self.host
        }
        return self._exec(cmd)

    def _exec(self, cmd):
        self._logger.debug('Executing: ' + cmd)
        sp = subprocess.Popen(cmd, shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        (out, err) = sp.communicate(input=None)

        self._logger.debug('cmd pid:%s' % sp.pid)
        self._logger.debug('cmd ret:%s' % sp.returncode)
        self._logger.debug('cmd out:%s' % out)
        self._logger.debug('cmd err:%s' % err)

        if sp.returncode == 0:
            return out.decode('utf-8')

        self._logger.error('Command failed: %s' % cmd)
        self._logger.error('cmd pid:%s' % sp.pid)
        self._logger.error('cmd ret:%s' % sp.returncode)
        self._logger.error('cmd out:%s' % out)
        self._logger.error('cmd err:%s' % err)

        raise Exception('cmd: %s failed: %s' % (cmd, err)) 

    def _map(self, what, constants):
        if what in constants:
            return constants[what]
        else:
            return constants['*']

