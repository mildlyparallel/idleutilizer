import operator
import xmlrpc.client
import xmltodict
import jmespath

from pprint import pprint as pp

from libs.clouds.one_constants import LCM_STATES

def init_handler(config, logger):
    return OpenNebula(config, logger)

def export_config():
    return (OpenNebula.section, OpenNebula.params)

class OpenNebula():
    section = 'backend.opennebula'

    params = [
        {
            'name': 'one_auth',
            'default': 'username:password',
            'type': str
        }, 
        {
            'name': 'endpoint',
            'default': 'http://10.10.10.10:2633/RPC2',
            'type': str
        },
        {
            'name': 'template_id',
            'default': '1174',
            'type': int
        },
        {
            'name': 'spawn_rpc_method',
            'default': 'one.template.instantiate',
            'type': str
        },
        {
            'name': 'spawn_rpc_args',
            'default': "[%(template_id)s, '%(name)s', False, '%(override)s']",
            'type': str
        },
        {
            'name': 'spawn_rpc_filter',
            'default': '',
            'type': str
        },
        {
            'name': 'remove_rpc_method',
            'default': 'one.vm.action',
            'type': str
        },
        {
            'name': 'remove_rpc_args',
            'default': "['delete', %(id)s]",
            'type': str
        },
        {
            'name': 'remove_rpc_filter',
            'default': '',
            'type': str
        },
        {
            'name': 'get_hosts_rpc_method',
            'default': 'one.vmpool.info',
            'type': str
        },
        # TODO: use vm range instead of fetching all VMs
        {
            'name': 'get_hosts_rpc_args',
            'default': '[-3, -1, 1, 3]',
            'type': str
        },
        {
            'name': 'get_hosts_rpc_filter',
            'default': "(VM_POOL.VM[] || [VM_POOL.VM])[?LCM_STATE=='3'].{id: to_number(ID), ip: TEMPLATE.NIC.IP}",
            'type': str
        },
        {
            'name': 'chown_rpc_method',
            'default': 'one.vm.chown',
            'type': str
        },
        {
            'name': 'chown_rpc_args',
            'default': "[%(id)s, %(uid)s, %(gid)s]",
            'type': str
        },
        {
            'name': 'chown_rpc_filter',
            'default': '',
            'type': str
        },
    ]

    def __init__(self, config, logger):
        values = config.get_values(OpenNebula.section, OpenNebula.params);
        for param in values:
            self.__dict__[param] = values[param]

        self._logger = logger

    def _get_method_fun(self, method):
        proxy = xmlrpc.client.ServerProxy(self.endpoint)
        return getattr(proxy, method)

    def _params_to_template(self, params):
        if not params:
            return ''

        template = ''

        for key in params:
            if key == 'memory':
                template += 'MEMORY=%s\\n' % str(params['memory'])
            elif key == 'cpu':
                template += 'CPU=%s\\n' % str(params['cpu'])
                template += 'VCPU=%s\\n' % str(params['cpu'])

        return template

    def spawn_host(self, params = None):
        template = self._params_to_template(params)

        if params and 'name' in params:
            name = params['name']
        else:
            name = 'ui-vm'

        args   = self.spawn_rpc_args % {
            'name': name,
            'override': template,
            'template_id': self.template_id
        }

        method = self.spawn_rpc_method
        filter = self.spawn_rpc_filter

        spawned = self._rpc_call(method, args, filter)

        if spawned == None:
            message = 'Failed to spawn a new host'
            self._logger.warning(message)
            raise Exception(message)

        if type(spawned) != int:
            raise Exception('Host id is expected to be int, got {} ({})'
                .format(spawned, str(type(spawned))))

        return spawned

    def remove_host(self, id_):
        if type(id_) != int:
            raise Exception('Host id is expected to be int, got {} ({})'
                .format(id_, str(type(id_))))

        self._logger.debug('Removing host #{}'.format(id_))

        method = self.remove_rpc_method
        args   = self.remove_rpc_args % {'id': id_}
        filter = self.remove_rpc_filter
        
        removed = self._rpc_call(method, args, filter)

        if removed == None:
            message = 'Failed to remove host #{}'.format(id_)
            self._logger.warning(message)
            raise Exception(message)

        if type(removed) != int:
            raise Exception('Host id is expected to be int, got {} ({})'
                .format(removed, str(type(removed))))

        return removed

    def get_hosts(self):
        method = self.get_hosts_rpc_method
        args   = self.get_hosts_rpc_args 
        filter = self.get_hosts_rpc_filter

        hosts = self._rpc_call(method, args, filter)

        if type(hosts) != list:
            message = 'Failed to get host list'
            self._logger.warning(message)
            raise Exception(message)

        for h in hosts:
            if ('id' not in h) or (type(h['id']) != int) or ('ip' not in h) or (type(h['ip']) != str):
                # TODO: don't throw, some VMs may have no IP
                raise Exception("Host list entry is expected to be {{'id': int, 'ip': str}}, got {}".format(h))

        return hosts

    def chown_host(self, id_, owner):
        if type(owner) != dict or 'uid' not in owner or 'gid' not in owner:
            raise Exception('Incorret owner format, expected {\'uid\': int, \'gid\': int}')

        uid = owner['uid']
        gid = owner['gid']

        if uid == None:
            uid = -1
        if gid == None:
            gid = -1

        if type(id_) != int or type(gid) != int or type(uid) != int:
            raise Exception('VM id, uid and gid are expected to be int , got id:{} uid:{}, gid:{}'
                .format(str(type(id_)), str(type(uid)), str(type(gid))))

        method = self.chown_rpc_method
        args   = self.chown_rpc_args % {'id': id_, 'uid': uid, 'gid': gid}
        filter = self.chown_rpc_filter

        self._logger.debug('Chown host #{} to uid:{} gid:{}'
            .format(id_, uid, gid))

        ret = self._rpc_call(method, args, filter)

        if ret == None:
            message = 'Failed to remove host #{}'.format(id_)
            self._logger.warning(message)
            raise Exception(message)

        if type(ret) != int:
            raise Exception('Host id is expected to be int, got {} ({})'
                .format(ret, str(type(ret))))

        return ret

    def _rpc_call(self, method, args, filter = None):
        args = eval(args)

        self._logger.debug('RPC {}({})'.format(method, args));

        call = self._get_method_fun(method)

        try:
            output = call(self.one_auth, *args)

            self._logger.debug('RPC {} returns: '.format(method, output))

            ok, payload, error_code, *_ = output
        except Exception as e:
            self._logger.error(
                'RPC {}({}) failed with exception'
                .format(method, args, str(e))
            )
            raise e

        if not ok:
            message = 'RPC {}({}) failed: code: {}, message: {}'.format(
                method, args, error_code, payload)
            self._logger.error(message)
            raise Exception(message)

        if type(payload) == int:
            return payload

        data = xmltodict.parse(payload, encoding='utf-8', dict_constructor=dict)

        if not filter:
            return data

        filtered = jmespath.search(filter, data)

        if filtered == None:
            raise Exception('JMESPath search with filter {} returns None'.format(filter))

        return filtered

